//
// /**
//  * First we will load all of this project's JavaScript dependencies which
//  * includes Vue and other libraries. It is a great starting point when
//  * building robust, powerful web applications using Vue and Laravel.
//  */
//
// require('./bootstrap');
//
// window.Vue = require('vue');
//
// /**
//  * The following block of code may be used to automatically register your
//  * Vue components. It will recursively scan this directory for the Vue
//  * components and automatically register them with their "basename".
//  *
//  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
//  */
//
// // const files = require.context('./', true, /\.vue$/i)
// // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
//
// // Vue.component('chat-message', require('./components/ChatMessage.vue').default);
// // Vue.component('chat-log', require('./components/ChatLog.vue').default);
// // Vue.component('chat-composer', require('./components/ChatComposer.vue').default);
// // Vue.component('online-users', require('./components/OnlineUser.vue').default);
// // Vue.component('addSubject', {
// //   addSubject: function(student_id) {
// //     this.$http({
// //         url: 'http://localhost:8000/message_userid',
// //         data: { studentId: student_id },
// //         method: 'POST'
// //     })
// //     .then(function(response) {
// //       console.log(response);
// //         // You've already attached the section to the student, so you can safely assume this is ok
// //         // this.reservations.push(section).
// //     }, function(response) {
// //         console.log('failed');
// //     });
// //   }
// // });
// // Vue.component('example-component', require('./components/ExampleComponent.vue').default);
//
// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */
//
// // const app = new Vue({
// //     el: '#app' ,
// //     data: {
// //       messages: [],
// //       usersInRoom: [],
// //       onlineUsers: []
// //     },
// //     // define data, methods here
// //
// //     methods: {
// //       addMessage(message){
// //         // console.log(message);
// //         this.messages.push(message);
// //         axios.post('/messages',message).then(response => {
// //           // console.log('done');
// //           // this.messages = response.data ;
// //         });
// //       },
// //
// //     },
// //     created(){
// //       // axios.get('/users',users).then(response => {
// //       //   console.log(users);
// //       //   // this.messages = response.data ;
// //       // });
// //       axios.get('/messages/all').then(response => {
// //         this.messages = response.data ;
// //         // console.log(response);
// //       });
// //
// //       window.Echo.channel('chatroom'+window.user_id)
// //       // alert(window.user_id)
// //       .listen('MessageCreated',(e)=>{
// //         console.log(e);
// //         Event.$emit(this.messages.push({
// //           message: e.message.message,
// //           user: e.user
// //         }));
// //         //handle event
// //       });
// //
// //
// //       Echo.join('onlineUsers')
// //       .here((users) => {
// //           Event.$emit(this.usersInRoom = users);
// //       })
// //       .joining((user) => {
// //           Event.$emit(this.usersInRoom.push(user));
// //       })
// //       .leaving((user) => {
// //           Event.$emit(this.usersInRoom = this.usersInRoom.filter(u => u != user));
// //       })
// //     }
// //
// // });
