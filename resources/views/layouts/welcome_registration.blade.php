<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CUHP</title>
    <link rel="icon" href="{{ asset('images/icons/favicon.png') }}" type="image/png">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>

<body class="dashboard-body">
    <div id="app" class="dashboard">
        <div class="dashboard-wrapper">

            <div class="dashboard-left">

              <div class="sidebar">
                  <span class="icon-close">
                      <i></i>
                      <i></i>
                  </span>
                  <a href="{{ url('/student') }}" class="logo">
                      <img src="{{ asset('images/logo.png') }}" alt="" />
                  </a>
                  <div class="sidenav">
                      @php
                          $directoryURI = $_SERVER['REQUEST_URI'];
                          $path = parse_url($directoryURI, PHP_URL_PATH);
                          $components = explode('/', $path);
                          $second_path="";
                          if (array_key_exists(2, $components)) {
                            $second_path=$components[2];
                          }
                      @endphp
                      <ul>
                        <li class="{{$second_path=='' ? ' active ' : ''}}">
                            <a href="/student" class="link-dashboard">Dashboard</a>
                        </li>
                          <li class="{{$second_path=='profile' ? ' active ' : ''}}">
                              <a href="/student/profile" class="link-profile">Profile</a>
                          </li>
                          <li class="{{$second_path=='forms_student' ? ' active ' : ''}}">
                              <a href="/register/forms_student" class="link-apply">Apply now</a>
                          </li>
                          <li class="{{$second_path=='courses' ? ' active ' : ''}}">
                              <a href="/student/courses" class="link-admitcard">Admit Card</a>
                          </li>
                          <li class="{{$second_path=='result' ? ' active ' : ''}}">
                              <a href="/student/dates" class="link-exam">Exam Date</a>
                          </li>
                          <li class="{{$second_path=='achievements' ? ' active ' : ''}}">
                              <a href="/student/result" class="link-results">Results</a>
                          </li>
                          <li>
                              <a class="dropdown-item nav-link" href="{{ route('logout') }}"
                                  onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();" title="Logout">
                                   Logout
                              </a>
                          </li>
                      </ul>
                  </div>

              </div>

            </div>

            <div class="dashboard-right">

                <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                    <div class="container">
<!--
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>
-->

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <ul class="navbar-nav mr-auto">

                            </ul>

                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav">
                                <!-- Authentication Links -->
                               <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        <span class="profile-img">
                                            <img src="{{ asset('images/icons/icon-profile.png') }}" />
                                        </span>
                                        {{Auth::user()->name}}
                                      @auth ('teacher')
                                        {{Auth::guard('teacher')->user()->name}}
                                      @endauth
                                      @auth ('management')
                                        {{Auth::guard('management')->user()->name}}
                                      @endauth
                                      @auth ('admin')
                                        {{Auth::guard('admin')->user()->name}}
                                      @endauth <span class="caret"></span>
                                    </a>
                                   <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a href="/student/profile" class="dropdown-item">Profile</a>
                                        <a href="/student/courses" class="dropdown-item">Admit Card</a>
                                        <a href="/student/result" class="dropdown-item">Exam Date</a>
                                        <a href="/student/achievements" class="dropdown-item">Results</a>
                                   </div>
                                </li>
                                <li class="nav-item">
                                    <a class="dropdown-item nav-link" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" title="Logout">
                                       &nbsp;
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                       @csrf
                                    </form>

                                </li>
                            </ul>
                        </div>

                        <span class="icon-menu">
                            <i></i>
                            <i></i>
                            <i></i>
                        </span>

                    </div>
                </nav>
                <div id="dashboardRightScroll" class="content">
                    @yield('content')
                </div>
            </div>

        </div>
    </div>
        <script  src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>
            $(document).ready(function() {
                $(".icon-menu").click(function(){
                      $(this).toggleClass("close");
                      $(".dashboard-left").toggleClass("open");
                      $(".dashboard-right").toggleClass("shrink");
                });

                $(".icon-close").click(function(){
                    $(".icon-menu").removeClass("close");
                    $(".dashboard-left").removeClass("open");
                    $(".dashboard-right").removeClass("shrink");
                });
            });

        </script>

<!--
        <script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
-->
        <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
        @yield('script')

    </body>

</html>
