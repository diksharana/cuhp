<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CUHP</title>
    <link rel="icon" href="{{ asset('images/icons/favicon.png') }}" type="image/png">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" defer></script>

</head>
<body class="dashboard-body">
    <div id="app" class="dashboard">
        <div class="dashboard-wrapper">

            <div class="dashboard-left">
              <div class="sidebar">
                  <a href="{{ url('/') }}" class="logo">
                      <img src="{{ asset('images/logo.png') }}" alt="" />
                  </a>
                  <div class="sidenav" >
                      <ul>
                          <li>
                              <a href="/student/profile" class="link-profile">Profile</a>
                          </li>
                          <li>
                              <a href="/student/attendance" class="link-attendance">Attendance</a>
                          </li>
                          <li>
                              <a href="/student/courses" class="link-course">Courses</a>
                          </li>
                          <li>
                              <a href="/student/result" class="link-result">Result</a>
                          </li>
                          <li>
                              <a href="/student/achievements" class="link-achievements">Achievements</a>
                          </li>
                          <li>
                              <a href="/student/assignments" class="link-assignments">Assignments</a>
                          </li>
                          <li>
                              <a href="/student/dates" class="link-exam">Exam Dates</a>
                          </li>
                          <li>
                              <a href="/student/admission" class="link-admission">Admission</a>
                          </li>
                          <li>
                              <a href="/student/timetable" class="link-timetable">Time Table</a>
                          </li>
                          <li>
                              <a href="/student/calendar" class="link-events">Calendar</a>
                          </li>
                          <li>
                              <a href="/student/messages" class="link-messages">Messages</a>
                          </li>
                          <li>
                              <a href="/student/other" class="link-others">Others</a>
                          </li>
                      </ul>
                  </div>
              </div>

            </div>
            <div class="dashboard-right">

                <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                    <div class="container">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <ul class="navbar-nav mr-auto">

                            </ul>

                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav">
                                <!-- Authentication Links -->
                               <li class="nav-item">
                                    <a id="navbarDropdown" class="nav-link" href="#" v-pre>
                                        <span class="profile-img">
                                            <img src="{{ asset('images/icons/icon-profile.png') }}" />
                                        </span>
                                      @auth ('student')
                                        {{Auth::guard('student')->user()->name}}
                                      @endauth
                                      @auth ('teacher')
                                        {{Auth::guard('teacher')->user()->name}}
                                      @endauth
                                      @auth ('management')
                                        {{Auth::guard('management')->user()->name}}
                                      @endauth
                                      @auth ('admin')
                                        {{Auth::guard('admin')->user()->name}}
                                      @endauth <span class="caret"></span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="dropdown-item nav-link" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" title="Logout">
                                       &nbsp;
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                       @csrf
                                    </form>

                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                @yield('content')
            </div>

        </div>
    </div>
</body>
</html>
