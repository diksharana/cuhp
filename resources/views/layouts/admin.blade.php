<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CUHP</title>
    <link rel="icon" href="{{ asset('images/icons/favicon.png') }}" type="image/png">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" defer></script>

</head>
<body class="dashboard-body admin-dashboard">
    <div id="app" class="dashboard">
        <div class="dashboard-wrapper">

            <div class="dashboard-left">
              <div class="sidebar">
                  <span class="icon-close">
                      <i></i>
                      <i></i>
                  </span>
                  <a  class="logo">
                      <img src="{{ asset('images/logo.png') }}" alt="" />
                  </a>
                  <div class="sidenav" >
                      @php
                          $directoryURI = $_SERVER['REQUEST_URI'];
                          $path = parse_url($directoryURI, PHP_URL_PATH);
                          $components = explode('/', $path);
                          $second_path="";
                          if (array_key_exists(2, $components)) {
                            $second_path=$components[2];
                          }
                      @endphp
                      <ul>
                          <li class="{{$second_path=='users' ? ' active ' : ''}}">
                              <a href="/cuhp-admin/users" class="link-profile">Student Verification</a>
                          </li>
                          <li class="{{$second_path=='approved' ? ' active ' : ''}}">
                              <a href="/cuhp-admin/approved" class="link-approved">Approved</a>
                          </li>
                          <li class="{{$second_path=='rejected' ? ' active ' : ''}}">
                              <a href="/cuhp-admin/rejected" class="link-rejected">Rejected</a>
                          </li>
                          <li class="{{$second_path=='user_admit' ? ' active ' : ''}}">
                              <a href="/cuhp-admin/user_admit" class="link-admitcard">Admit Cards</a>
                          </li>
                          <li class="{{$second_path=='exam_schedule' ? ' active ' : ''}}">
                              <a href="/cuhp-admin/exam_schedule" class="link-exam">Exam Dates</a>
                          </li>
                          <li class="{{$second_path=='result' ? ' active ' : ''}}">
                              <a href="/cuhp-admin/result" class="link-results">Result</a>
                          </li>
                          <li class="{{$second_path=='send_emails' ? ' active ' : ''}}">
                              <a href="/cuhp-admin/send_emails" class="link-results">Send Emails</a>
                          </li>
                          <li>
                              <a class="link-logout" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();" class="link-logout">
                                            Logout
                              </a>
                          </li>
                      </ul>
                  </div>
              </div>

            </div>
            <div class="dashboard-right">

                <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                    <div class="container">
<!--
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>
-->

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <ul class="navbar-nav mr-auto">

                            </ul>

                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav ml-auto">
                                <!-- Authentication Links -->
                               <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                      Admin
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a href="/cuhp-admin/users" class="dropdown-item">Student Verification</a>
                                        <a href="/cuhp-admin/approved" class="dropdown-item">Approved</a>
                                        <a href="/cuhp-admin/rejected" class="dropdown-item">Rejected</a>
                                        <a href="/cuhp-admin/user_admit" class="dropdown-item">Admit Cards</a>
                                        <a href="/cuhp-admin/exam_schedule" class="link-exam">Exam Dates</a>
                                        <a href="/cuhp-admin/result" class="link-results">Result</a>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();" class="link-logout">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <span class="icon-menu">
                            <i></i>
                            <i></i>
                            <i></i>
                        </span>

                    </div>
                </nav>
                <div class="content">
                    @yield('content')
                </div>
            </div>

        </div>
    </div>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".icon-menu").click(function(){
                  $(this).toggleClass("close");
                  $(".dashboard-left").toggleClass("open");
                  $(".dashboard-right").toggleClass("shrink");
            });

            $(".icon-close").click(function(){
                $(".icon-menu").removeClass("close");
                $(".dashboard-left").removeClass("open");
                $(".dashboard-right").removeClass("shrink");
            });
        });

    </script>
    @yield('script')
</body>
</html>
