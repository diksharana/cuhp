<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CUHP</title>
    <link rel="icon" href="{{ asset('images/icons/favicon.png') }}" type="image/png">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

</head>
<body class="welcome-body @yield('body_classname')">

    <div  class="welcome-box">
        <div class="welcome-header">
            <a href="" class="logo">
                <img src="{{ asset('images/logo.png') }}" alt="" />
            </a>
            <h2 class="welcome-box-title">Central University of Himachal Pradesh</h2>
        </div>
        @yield('content')
    </div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" defer></script>
<script src="{{ asset('js/app.js') }}" defer></script>

<script type="text/javascript">
$(document).ready(function() {
  $('#register').on('click',function() {
    $(".register-box").promise().done(function(){
          last=(window.location.href).split('/');
          last=last[last.length-1];
          //alert(last)
            (window.location.href = "/register/auth/"+last).animate({'left' : 'show'});
        });
    });
    $('#login').on('click',function() {
       $(".login-box").promise().done(function(){
         last=(window.location.href).split('/');
         last=last[last.length-1];
            (window.location.href = "/login/"+last).animate({'right' : 'show'});
       });
    });
  });

</script>
@yield('script')
</body>
</html>
