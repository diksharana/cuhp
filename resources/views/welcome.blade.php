@extends('layouts.app')

@section('content')

    <div class="welcome-content">

        <h1>
            <span>Welcome to</span>
            Central University
            <span>of</span>
            Himachal Pradesh
        </h1>
        <p>Click on the link below for your registration/login.</p>
        <a href="{{'register/auth/student' }}" class="btn btn- mt15">Continue</a>
        <div class="made-by">
            Designed by Developed by
            <img src="{{ asset('images/logo-amakein.png') }}" alt="" />
        </div>

        {{-- @if (Route::has('login')) --}}
            {{-- @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
              @if (Route::has('register'))
               {{-- <a href="{{'register/auth/student' }}">Register</a> --}}
                {{-- <a href="{{'/register/teacher' }}">Teacher Register</a>
                <a href="{{'/register/management' }}">Management Register</a>
                <a href="{{'/register/admin' }}">Admin Register</a>
              @endif --}}
              {{-- <select>
                <option value="{{'/login/student'}}">Volvo</option>
                <option value="{{'/login/teacher' }}">Saab</option>
                <option value="{{'/login/management' }}">Mercedes</option>
                <option value="{{'/login/admin' }}">Audi</option>
              </select> --}}
                {{-- <a href="{{'/login/student'}}">Login</a> --}}
                {{-- <a href="{{'/login/teacher' }}">Teacher Login</a>
                <a href="{{'/login/management' }}">Management Login</a>
                <a href="{{'/login/admin' }}">Admin Login</a> --}}

            {{-- @endauth
        @endif --}}

    </div>


@endsection
