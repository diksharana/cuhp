@extends('layouts.admin')
  @section('content')

  <form class="admitCard-form" action="" id="formOne" method="post" novalidate>
    @csrf
    <table class="table" cellspacing="0" cellpadding="0">
          <thead>
              <tr>
                  <th colspan="2">
                      <img src="{{asset('images/cu-hp.png')}}" alt="" />
                      <span>
                          Central University of Himachal Pradesh
                      </span>
                  </th>
              </tr>
          </thead>
          <tbody>
              <tr class="title-header">
                  <td colspan="2">
                      <span>Admit Card</span>
                  </td>
              </tr>
              <tr class="student-info">
                  <td>
                      <ul>
                          <li class="no-border">
                              <label>Student Name:</label>
                              <span>
                                  <input type="text" name="first_name" disabled value="{{$data[0]->first_name}} {{$data[0]->last_name}}">
                              </span>
                          </li>
                          <li>
                              <label>Student ID:</label>
                              <span class="studentID">
                                  <input type="hidden" name="student_id" value="{{$data[0]->userid}}">
                                  <input type="hidden" name="email" value="{{$data[0]->email}}">
                                  {{$data[0]->userid}}
                              </span>
                          </li>
                          <li>
                              <label>Date of Birth:</label>
                              <span><input type="text" name="DOB" disabled value="{{$data[0]->DOB}}"></span>
                          </li>
                          <li>
                              <label>Course:</label>
                              <span><input type="text" name="course" disabled value="{{$data[0]->course}}"></span>
                          </li>
                          <li>
                              <label>Centre Code:</label>
                              <span> <input type="text" name="centre_code" value="{{isset($data[0]->centre_code)?$data[0]->centre_code:''}}" required> </span>
                          </li>
                          <li>
                              <label>Centre Address:</label>
                              <span> <textarea name="centre_address" required>{{isset($data[0]->centre_address)?$data[0]->centre_address:''}}</textarea> </span>
                          </li>
                      </ul>
                  </td>
                  <td>
                      <span>
                          <img src="{{$data[0]->photo}}" alt="{{$data[0]->photo}}" />
                      </span>
                  </td>
              </tr>
              <tr class="exam-info">
                  <td colspan="2">
                      <ul>
                          <li>
                              <label>Date of Examination</label>
                              <span> <input type="date" name="date_of_examination" value="{{isset($data[0]->date_of_examination)?$data[0]->date_of_examination:''}}" required> </span>
                          </li>
                          <li>
                              <label>Examination Timing</label>
                              <span> <input type="time" name="examination_timings" value="{{isset($data[0]->examination_timings)?$data[0]->examination_timings:''}}" required> </span>
                          </li>
                          <li>
                              <label>Reporting Time</label>
                              <span> <input type="time" name="reporting_time" value="{{isset($data[0]->reporting_time)?$data[0]->reporting_time:''}}" required> </span>
                          </li>
                          <li class="no-border">
                              <label>Examination Controller</label>
                              <span> <input type="text" name="examination_controller" value="{{isset($data[0]->examination_controller)?$data[0]->examination_controller:''}}" required> </span>
                          </li>
                      </ul>
                  </td>
              </tr>
          </tbody>
      </table>
      @if (!isset($data[0]->examination_controller))
        <input type="submit" name="submit" class="btn approve" value="Approve">
      @else
        <div class="alert alert-success">
          Admit Card Approved!
        </div>
      @endif
  </form>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $("#formOne").submit(function(e){
    e.preventDefault();
    console.log($(this).serializeArray());
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('input[name="_token"]').val()
      },
      method:'post',
      url: '/approve/admitcard',
      data: new FormData($('#formOne')[0]),
      contentType: false,
      cache: false,
      processData: false,
      success: function(data) {
          console.log(data);
          location.reload();
      },
      error: function(error) {
          console.log(error);
      }
  });
});
});
</script>
