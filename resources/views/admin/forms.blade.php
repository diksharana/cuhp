@extends('layouts.admin')
  @section('content')
    <h2> Form </h2>
    <form class="" action="/save_form" method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      Form Name - <input type="text" name="form_name" value=""> <br/>
      Related to - <select class="" name="related_to">
        <option value="Student">Student</option>
        <option value="Teacher">Teacher</option>
        <option value="Management">Management</option>
      </select> <br/>
      File - <input type="file" name="url" value=""> <br/>
      <input type="submit" name="" value="Upload">
    </form>
  @endsection
