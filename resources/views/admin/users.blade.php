@extends('layouts.admin')
  @section('content')
    <h2>Verify Users</h2>
    {{-- <button type="button" name="button" value="Student" class="users">Verify Student</button> --}}
    <div class="table table-responsive">
      <table class="table page-list campaign-list">
        <thead>
          <tr>
            <th>ID</th>
            <th>Student Id</th>
            <th>Course</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>DOB</th>
            <th>Gender</th>
            <th>Address Line1</th>
            <th>Address Line2</th>
            <th>City</th>
            <th>State</th>
            <th>Code</th>
            <th>Country</th>
            <th>Correspondance Address Line1</th>
            <th>Correspondance Address Line2</th>
            <th>Correspondance City</th>
            <th>Correspondance State</th>
            <th>Correspondance Code</th>
            <th>Correspondance Country</th>
            <th>Email </th>
            <th>Phone Number </th>
            <th>Mother Name </th>
            <th>Father Name </th>
            <th>Parents Phone </th>
            <th>Highest Qualification </th>
            <th>Percentage in Highest Qualification </th>
            <th>Photo</th>
            <th>Adhar Card </th>
            <th>Signature</th>
            <th>Marksheet 10th</th>
            <th>Marksheet 12th</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($data as $key => $value)
          <tr>
          <td>{{$value->id }} </td>
          <td>{{$value->userid}} </td>
          <td>{{$value->course }} </td>
          <td>{{$value->first_name}} </td>
          <td>{{$value->last_name }} </td>
          <td>{{$value->DOB }} </td>
          <td>{{$value->gender}} </td>
          <td>{{$value->address_line1  }} </td>
          <td>{{$value->address_line2  }} </td>
          <td>{{$value->city  }} </td>
          <td>{{$value->state  }} </td>
          <td>{{$value->code  }} </td>
          <td>{{$value->country  }} </td>
          @if ($value->address_line1_correspondance=='')
            <td>{{$value->address_line1  }} </td>
            <td>{{$value->address_line2  }} </td>
            <td>{{$value->city  }} </td>
            <td>{{$value->state  }} </td>
            <td>{{$value->code  }} </td>
            <td>{{$value->country  }} </td>
          @else
            <td>{{$value->address_line1_correspondance  }} </td>
            <td>{{$value->address_line2_correspondance  }} </td>
            <td>{{$value->city_correspondance  }} </td>
            <td>{{$value->state_correspondance  }} </td>
            <td>{{$value->code_correspondance  }} </td>
            <td>{{$value->country_correspondance  }} </td>
          @endif
          <td>{{$value->email  }} </td>
          <td>{{$value->phone_number  }} </td>
          <td>{{$value->mother_name  }} </td>
          <td>{{$value->father_name  }} </td>
          <td>{{$value->parents_phone }} </td>
          <td>{{$value->highest_qualification  }} </td>
          <td>{{$value->percentage_highest_qualification  }} </td>
          <td>{{$value->photo  }} </td>
          <td>{{$value->adhar_card  }} </td>
          <td>{{$value->signature  }} </td>
          <td>{{$value->marksheet_10th  }} </td>
          <td>{{$value->marksheet_12th }} </td>
          <td><button type="button" name="button" >Approve</button>
          <button type="button" name="button" id="reject" >Reject</button> </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    </div>
    {{-- <button type="button" name="button" value="Teacher" class="users">Verify Teacher</button>
    <button type="button" name="button" value="Management" class="users">Verify Management</button> --}}
    {{-- <div class="user_data">

    </div> --}}
  @endsection
  {{-- <script src="https //cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('.users').on('click'
    function(e) {
      // alert('hi')
      $("div.user_data").empty();
      var value = $(this).val();
      axios.post('/verify',{
        // value:value
      })
      .then(function (response) {
        console.log(response.data.data);
        data = response.data.data;
        $.each(data,function(i,v){
          $('.user_data').append('<p>'+v.name+'</p>')
        })
      })
  });
});
  </script> --}}
