@extends('layouts.admin')
@section('content')
        <div class="user-list-title">
            <h2 class="content-title">Approved List</h2>
            <p>List of the approved students</p>
        </div>
        <div class="accordion users-list" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Approved Students
                        </button>
                    </h2>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="table application-form table-responsive">
                            <table class="table page-list campaign-list approved-list">
                                <thead>
                                    <tr>
                                        <th>Student Id</th>
                                        <th>Course</th>
                                        <th>State</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $value)
                                        <tr id="{{$value->email}}">
                                            <td>{{$value->id }} </td>
                                            <td>{{$value->course }} </td>
                                            <td>{{$value->state }} </td>
                                            <td>{{$value->email }} </td>
                                            <td>{{$value->phone_number}} </td>
                                            <td>
                                              <span>Approved</span>
                                              <a href="/cuhp-admin/view_application/{{$value->id}}" class="btn">View application</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
  @endsection
