@extends('layouts.admin')
  @section('content')

        <div class="user-list-title">
            <h2 class="content-title">Students List</h2>
            <p>List of the students who has applied for the registration.</p>
        </div>
        {{-- <button type="button" name="button" value="Student" class="users">Verify Student</button> --}}


        <div class="accordion users-list" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Registered Students
                        </button>
                    </h2>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="table application-form table-responsive">
                            <table class="table page-list campaign-list registered-list">
                                <thead>
                                    <tr>
                                        <th>Student Id</th>
                                        <th>Course</th>
                                        <th>First Name</th>
                                        <th>State</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($registered_users as $key => $value)
                                        <tr id="{{$value->email}}">
                                            <td>{{$value->id }} </td>
                                            <td>{{$value->course }} </td>
                                            <td>{{$value->name}} </td>
                                            <td>{{$value->state }} </td>
                                            <td>{{$value->email }} </td>
                                            <td>{{$value->phone_number}} </td>
                                            <td>
                                              {{'Application not submitted yet'}}
                                              {{-- <button type="button" name="button" class="btn user_detail">Open</button>  --}}
                                              {{-- <button type="button" name="button" id="reject">Reject</button>  --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          Applied Students
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="table application-form table-responsive">
                            <table class="table page-list campaign-list">
                                <thead>
                                    <tr>
                                        <th>Student Id</th>
                                        <th>Course</th>
                                        <th>First Name</th>
                                        <th>State</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($applied_users as $key => $value)
                                        <tr id="{{$value->email}}">
                                            <td>{{$value->id }} </td>
                                            <td>{{$value->course }} </td>
                                            <td>{{$value->name}} </td>
                                            <td>{{$value->state }} </td>
                                            <td>{{$value->email }} </td>
                                            <td>{{$value->phone_number}} </td>
                                            <td><button type="button" name="button" class="btn user_detail">View application</button> {{-- <button type="button" name="button" id="reject">Reject</button> </td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- single student info -->
        <div class="single-student-info">
            <h2 class="user-info-title">Student Name: <span class="single-student-name"></span></h2>
            <ul class="single-student">
                <li>
                    <label>Student Id</label>
                    <p class="single-student-id"></p>
                </li>
                <li>
                    <label>First Name</label>
                    <p class="single-student-first-name"></p>
                </li>
                <li>
                    <label>Last Name</label>
                    <p class="single-student-last-name"></p>
                </li>
                <li>
                    <label>Gender</label>
                    <p class="single-student-gender"></p>
                </li>
                <li>
                    <label>Phone Number</label>
                    <p class="single-student-phone"></p>
                </li>
                <li>
                    <label>Highest Qualification</label>
                    <p class="single-student-highest-qualification"></p>
                </li>
                <li>
                    <label>Percentage Highest Qualification</label>
                    <p class="single-student-percentage-highest-qualification"></p>
                </li>
                <li>
                    <label>Email</label>
                    <p class="single-student-email"></p>
                </li>
                <li>
                    <label>Course</label>
                    <p class="single-student-course"></p>
                </li>
                <li>
                    <label>Mother's Name</label>
                    <p class="single-student-mother"></p>
                </li>
                <li>
                    <label>Father's Name</label>
                    <p class="single-student-father"></p>
                </li>
                <li>
                    <label>Parent Phone</label>
                    <p class="single-student-parent-phone"></p>
                </li>
                <li>
                    <label>Photograph</label>
                    <a class="single-student-photo" target="_blank" href=" ">Photograph</a>
                </li>
                <li>
                    <label>12th Marksheet</label>
                    <a class="single-student-12-marksheet" target="_blank" href=" ">12th Marksheet</a>
                </li>
                <li>
                    <label>10th Marksheet</label>
                    <a class="single-student-10-marksheet" target="_blank" href=" ">10th Marksheet</a>
                </li>
                <li>
                    <label>Signature</label>
                  <a class="single-student-signature" target="_blank" href=" ">Signature</a>
                </li>
                <li>
                    <label>Adhar Card</label>
                    <a class="single-student-adhar" target="_blank" href=" ">Adhar Card</a>
                </li>
                <li>
                    <label>Address Line 1</label>
                    <p class="single-student-addressline1"></p>
                </li>
                <li>
                    <label>Address Line 2</label>
                    <p class="single-student-addressline2"></p>
                </li>
                <li>
                    <label>State</label>
                    <p class="single-student-state"></p>
                </li>
                <li>
                    <label>City</label>
                    <p class="single-student-city"></p>
                </li>
                <li>
                    <label>Country</label>
                    <p class="single-student-country"></p>
                </li>
                <li>
                    <label>Postal Code</label>
                    <p class="single-student-code"></p>
                </li>
                <li class="student-info-subtitle">
                    Correspondence Address
                </li>
                <li>
                    <label>Address Line 1</label>
                    <p class="single-student-addressline1-correspondance"></p>
                </li>
                <li>
                    <label>Address Line 2</label>
                    <p class="single-student-addressline2-correspondance"></p>
                </li>
                <li>
                    <label>City</label>
                    <p class="single-student-city-correspondance"></p>
                </li>
                <li>
                    <label>State</label>
                    <p class="single-student-state-correspondance"></p>
                </li>
                <li>
                    <label>Country</label>
                    <p class="single-student-country-correspondance"></p>
                </li>
                <li>
                    <label>Postal Code</label>
                    <p class="single-student-code-correspondance"></p>
                </li>
                <li class="student-action">
                    <h6>Take Action</h6>
                    <form>
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea type="text" id="reject_reason" class="form-control"></textarea>
                            <button type="button" id="reject" class="btn">Reject</button>
                        </div>
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea type="text" id="approve_reason" class="form-control"></textarea>
                            <button type="button" id="approve" class="btn">Approve</button>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
  @endsection
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('.user_detail').on('click', function(e) {
      // alert($(this).closest('tr').attr('id'))
      var value = $(this).closest('tr').attr('id');
      axios.post('/complete_detail',{
        value:value
      })
      .then(function (response) {
        console.log(response.data[0]);
        $('.accordion').hide();
        $('.user-list-title').hide();
        $('.single-student-info').show();
        $(".single-student-id").append(response.data[0].userid);
        $(".single-student-name").append(response.data[0].name);
        $('.single-student-first-name').append(response.data[0].first_name);
        $('.single-student-last-name').append(response.data[0].last_name);
        $('.single-student-gender').append(response.data[0].gender);
        $('.single-student-phone').append(response.data[0].phone_number);
        $('.single-student-highest-qualification').append(response.data[0].highest_qualification);
        $('.single-student-percentage-highest-qualification').append(response.data[0].percentage_highest_qualification);
        $('.single-student-email').append(response.data[0].email);
        $('.single-student-course').append(response.data[0].course);
        $('.single-student-mother').append(response.data[0].mother_name);
        $('.single-student-father').append(response.data[0].father_name);
        $('.single-student-parent-phone').append(response.data[0].parents_phone);
        $('.single-student-photo').attr("href",response.data[0].photo);
        $('.single-student-12-marksheet').attr("href",response.data[0].marksheet_12th);
        $('.single-student-10-marksheet').attr("href",response.data[0].marksheet_10th);
        $('.single-student-signature').attr("href",response.data[0].signature);
        $('.single-student-adhar').attr("href",response.data[0].adhar_card);
        $('.single-student-addressline1').append(response.data[0].address_line1);
        $('.single-student-addressline2').append(response.data[0].address_line2);
        $('.single-student-state').append(response.data[0].state);
        $('.single-student-city').append(response.data[0].city);
        $('.single-student-country').append(response.data[0].country);
        $('.single-student-code').append(response.data[0].code);
        $('.single-student-addressline1-correspondance').append(response.data[0].address_line1_correspondance);
        $('.single-student-addressline2-correspondance').append(response.data[0].address_line2_correspondance);
        $('.single-student-city-correspondance').append(response.data[0].city_correspondance);
        $('.single-student-state-correspondance').append(response.data[0].city_correspondance);
        $('.single-student-country-correspondance').append(response.data[0].country_correspondance);
        $('.single-student-code-correspondance').append(response.data[0].code_correspondance);
        if (response.data[0].status=='approved') {
          $('.student-action').empty();
          $('.student-action').append('<h1>Approved</h1>');
        }
        else if(response.data[0].status=='rejected'){
          $('.student-action').empty();
          $('.student-action').append('<h1>Rejected</h1>');
        }
      });
    });
    $('#reject').on('click',function(e){
      var reason = $('textarea#reject_reason').val();
      var email = $('.single-student-email').text();
      axios.post('/reject_student',{
        reason:reason,
        email:email
      })
      .then(function (response) {
        window.location = "/cuhp-admin/users"
      });
    });
    $('#approve').on('click',function(e){
      var email = $('.single-student-email').text();
      axios.post('/approve_student',{
        email:email
      })
      .then(function (response) {
        window.location = "/cuhp-admin/users"
      });
    });
  });
  </script>
