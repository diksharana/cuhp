@extends('layouts.admin')
  @section('content')
        @if (session()->has('message'))
          <h5 class="alert alert-success">{{ session('message') }}</h5>
        @endif
        <h2 class="content-title">Exam Schedule</h2>
        @if (count($result))
          <div class="application-form table-responsive exam-list">
              <table class="table">
                  <thead>
                      <tr>
                          <th>Exam Name</th>
                          <th>Uploaded On</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($result as $key => $value)
                          <tr id="{{$value->id}}">
                              <td><a href="{{asset($value->url) }}" target="_blank">{{$value->form_name }} </a></td>
                              <td>{{$value->created_at }} </td>
                          </tr>
                      @endforeach
                  </tbody>
              </table>
          </div>
        @endif
        <form class="form-schedule" action="/schedule" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
            <p class="form-info">
                Upload the PDF file below for the entrance exam schedule.<br/>
                When you click on the upload button it will be uploaded in every student's dashboard with a notification.
            </p>
            <div class="form-group">
              <input type="text" name="form_name" value="" placeholder="Enter File Name" class="form-control" required>
              <input type="file" id="exam_file" name="url" value="" class="form-control-file" required>
              @if ($errors->has('url'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('url') }}</strong>
                  </span>
              @endif
              <input type="hidden" name="data" value="datesheet">
              <input type="submit" name="" value="Upload" class="btn">
            </div>
        </form>
  @endsection
  @section('script')
    <script type="text/javascript">
      $(document).ready(function(){
        $('.btn').on('click',function(e){
          if (!$('#exam_file').val()) {
            alert('please select file');
            return;
          }
        });
      });
    </script>
  @endsection
