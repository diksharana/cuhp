@extends('layouts.app')
@section('body_classname')
{{'admin-login'}}
@endsection
@section('content')
    <div class="welcome-content">

        <div class="admin-login-box">
            <div class="admin-login-title">Admin Login</div>
            @if (\Session::has('message'))
              <div class="alert alert-danger">
                {!! \Session::get('message') !!}
              </div>
            @endif
            <form class="" action="/cuhp-admin/login" method="post">
                {{ csrf_field() }}
                <div class="form-group field-email">
                    <input type="email" name="email" value="" class="form-control" placeholder="Email">
                </div>
                <div class="form-group field-password">
                    <input type="password" name="password" value="" class="form-control" placeholder="Password">
                </div>
                <input type="submit" value="Log In" class="btn btn-primary">
            </form>
        </div>

        <div class="made-by">
            Designed by Developed by
            <img src="http://127.0.0.1:8000/images/logo-amakein.png" alt="">
        </div>

    </div>
@endsection
