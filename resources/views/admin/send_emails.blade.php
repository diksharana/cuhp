@extends('layouts.admin')
  @section('content')
        <h2 class="content-title">Send Emails</h2>
        <form class="form-schedule" method="post" enctype="multipart/form-data">
          <div class="loader"></div>
          {{ csrf_field() }}
            <div class="form-group">
                <h6>Send emails to registered users to submit application before last date.</h6>
                <input type="submit" name="" value="Send mails" class="btn">
            </div>
        </form>
  @endsection
  @section('script')
    <script type="text/javascript">
      $(document).ready(function(){
        $('.btn').on('click',function(e){
            e.preventDefault();
            $('.loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                },
                method:'post',
                url: '/cuhp-admin/send_emails/complete_process',
                success: function(data) {
                    console.log(data);
                    $('.loader').hide();
                },
                error: function(error) {
                    console.log(error);
                    $('.loader').hide();
                }
            });
        });
      });
    </script>
  @endsection
