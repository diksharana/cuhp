@extends('layouts.admin')
  @section('content')
    <h2 class="content-title">Admit Cards</h2>
    <p>Kindly enter centre details</p>
    {{-- <button type="button" name="button" value="Student" class="users">Verify Student</button> --}}
      <div class="table application-form table-responsive">
        <table class="table page-list campaign-list">
        <thead>
          <tr>
            <th>Student Id</th>
            <th>Course</th>
            <th>First Name</th>
            <th>State</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($data as $key => $value)
            <tr id="{{'data_'.$value->id}}">
                <td>{{$value->id }} </td>
                <td>{{$value->course }} </td>
                <td>{{$value->first_name}} </td>
                <td>{{$value->state  }} </td>
                <td>{{$value->email  }} </td>
                <td>{{$value->phone_number}} </td>
                <form class="" action="/cuhp-admin/admitcard" method="post">
                  @csrf
                  <input type="hidden" name="student_id" value="{{$value->userid}}">
                <td>
                  @if ($value->centre_code)
                    {{'Approved'}}
                  @else
                    <button type="submit" name="button" class="btn user_detail">Open</button>
                  @endif
                  </form>
                {{-- <button type="button" name="button" id="reject" >Reject</button> </td> --}}
            </tr>
          @endforeach
        </tbody>
      </table>
      </div>
  @endsection
