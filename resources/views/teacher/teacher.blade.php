 {{-- resources/views/writer.blade.php --}}

@extends('layouts.teacher')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Teacher Dashboard</div>

                <div class="card-body">
                  @auth ('teacher')
                    Hey {{Auth::guard('teacher')->user()->name}} !!
                  @endauth
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
