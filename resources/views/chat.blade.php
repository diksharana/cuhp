@extends('layouts.student')

@section('content')

<div class="container">
  <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>

    <div class="row justify-content-center" style="display:block; margin-top: 1%">
      <div id="app">
        <div class="panel-heading" style="border:1px solid black; padding: 0 2%; width: 70%;float:left;">
          <div class="head" style="border-bottom:1px solid black; text-align: center; background-color: #3490dc">Inbox</div>
          <chat-log :messages="messages" ></chat-log>
          <chat-composer v-on:messagesent="addMessage"></chat-composer>
      </div>
    </div>
      <div class="panel-side-heading" style="border:1px solid black;float:right; width: 20%;padding: .5%">
        <div class="head" style="border-bottom:1px solid black; text-align: center; background-color: #3490dc">Users [@{{ usersInRoom.length }}]</div>
        <online-users></online-users>
          {{-- <select class="student" name="select">
            @foreach ($students as $key => $value)
              <option value="{{$value->roll_number}}">{{$value->name}}</option>
              {{-- <a href="/messages/"></a> --}}
              {{-- <online-users v-bind:friends = "{{$students}}" v-bind:online-users="onlineUsers"></online-users>
               - [{{$value->roll_number}}]
               @if ($value->avatar=='sample.jpg')
                <img src="{{asset('sample.jpg')}}" width="40px" height="40px" alt="{{asset('sample.jpg')}}"><br>
                @else
                <img src="{{$value->avatar}}" width="40px" height="40px" alt="{{$value->avatar}}"><br>
              @endif
          <br>
         @endforeach
       </select> --}}
      </div>
      {{-- <div class="panel-side-heading" style="border:1px solid black;float:right; margin-right: 5%; width: 10%;padding: .5%">
        <div class="head" style="border-bottom:1px solid black; text-align: center; background-color: #3490dc">Teacher</div>
        <select class="teacher" name="select">
        @foreach ($teachers as $key => $value)
        <option value="{{$value->roll_number}}">{{$value->name}}</option>
          <br>
        @endforeach
      </div> --}}
  </div>
    {{-- @foreach ($students as $key => $value)
  <button v-on:click="addSubject({{ $value->id}}, )"
        class="btn btn-xs btn-primary"
        >Add</button>
@endforeach --}}
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('.student').on('click',function(e) {
    e.preventDefault();
    var student = $('.student').val();
    console.log(student);
    axios({
      method: 'post',
      url: '/chat_of_student',
      data: {student : student},
      contentType: false,
      cache: false,
      processData: false,
      config: { headers: {'Content-Type': 'multipart/form-data' }}
    })
    .then(function (response) {
        console.log(response);
        location.reload();
    })
    .catch(function (response) {
        console.log(response);
    });
  });
});
</script>
