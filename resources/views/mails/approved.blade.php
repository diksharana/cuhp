<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CUHP Newsletter</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
    <style>
        * {
            font-family: 'Lato', sans-serif;
            box-sizing: border-box;
        }

        .wrapper {
            table-layout: fixed;
            width: 800px;
        }

        .header {
            background: #099742;
        }

        .header th {
            padding: 50px 0;
        }

        .logo {
            display: inline-block;
            width: 130px;
            height: 130px;
            border-radius: 100%;
            background: #fff;
        }

        .logo img {
            width: 90px;
            margin-top: 20px;
        }

        .header-content {
            background: #099742;
        }

        .header-content .content {
            width: 600px;
            overflow: hidden;
            margin: 0 auto;
            padding: 50px;
            border-top: 5px solid #eee;
            border-right: 5px solid #eee;
            border-left: 5px solid #eee;
            background: #fff;
        }

        .header-content .content h1 {
            font-size: 40px;
            line-height: 44px;
            font-weight: 400;
            color: #099742;
            margin: 0;
            text-align: center;
        }

        .header-content .content h1 span {
            display: block;
            font-size: 24px;
            color: #212121;
        }

        .content-wrapper {
            width: 600px;
            overflow: hidden;
            margin: 0 auto;
            padding: 50px;
            border-right: 5px solid #eee;
            border-bottom: 5px solid #eee;
            border-left: 5px solid #eee;
            background: #fff;
        }

        .content-wrapper h2 {
            font-size: 20px;
            line-height: 30px;
            font-weight: 700;
            margin: 0;
        }

        .content-wrapper p {
            font-size: 16px;
            line-height: 24px;
            margin: 20px 0 50px;
        }

        .content-wrapper h3 {
            font-size: 18px;
            line-height: 24px;
            font-weight: 700;
            margin: 15px 0 5px;
        }

        .content-wrapper h4 {
            font-size: 18px;
            line-height: 24px;
            font-weight: 700;
            margin: 0;
        }

        .content-wrapper .btn {
            font-size: 16px;
            line-height: 24px;
            color: #fff;
            cursor: pointer;
            margin: 15px 0;
            padding: 5px 25px;
            border: 0;
            border-radius: 4px;
            background: #099742;
        }

        .footer {
            font-size: 14px;
            line-height: 20px;
            color: #555;
            margin: 30px 0;
            text-align: center;
        }

    </style>
</head>

<body>
    <table cellspacing="0" cellpadding="0" class="wrapper">
        <thead>
            <tr class="header">
                <th>
                    <div class="logo">
                        <img src="http://127.0.0.1:8000/images/logo.png" alt="" />
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="header-content">
                        <div class="content">
                            <h1>
                                <span>Welcome to</span> Central University
                                <span>of</span> Himachal Pradesh
                            </h1>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="content-wrapper">
                        <h2>Dear Candidate,</h2>
                        <p> Your candidature is successfully approved. We welcome you to the university. </p>
                        {{-- <button type="button" class="btn">Button</button> --}}
                        <h3>Thanks</h3>
                        <h4>Central University of Himachal Pradesh</h4>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="footer">© Central University of Himachal Pradesh</div>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
