{{-- resources/views/writer.blade.php --}}

@extends('layouts.management')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Management Dashboard</div>

                <div class="card-body">
                  @auth ('management')
                    Hey {{Auth::guard('management')->user()->name}} !!
                  @endauth
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
