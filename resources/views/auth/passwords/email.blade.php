@extends('layouts.app')
@section('body_classname')
{{'student-register'}}
@endsection
@section('content')

<div class="welcome-content">
    <div class="reset-box">
    
        <div class="title">Forgot Password?</div>
        <p>Please enter your email address to request a password reset.</p>
        <div class="login-register-form">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="form-group field-email">

                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('Email Address') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <div class="clearfix"></div>
                <a href="{{'/register/auth/student'}}" class="text-link">Login</a>
                
            </form>
        </div>
        
    </div>
</div>
@endsection
