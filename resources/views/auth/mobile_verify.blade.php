@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Mobile Number') }}</div>

                <div class="card-body">
                  <form class="student-registration-form" action="verify-user">
                    @csrf
                      <div class="form-group">
                          <label class="form-heading">Enter your OTP</label>
                      </div>
                      <div class="form-group">
                          <label>Enter your otp</label>
                          <input type="text" name="code" value="" class="form-control">
                      </div>
                      <input type="submit" name="" value="Next" class="btn">
                  </form>
                    {{-- @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif --}}

                    {{-- {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>. --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
