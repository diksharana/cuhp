@extends('layouts.app')

@section('body_classname')
{{'student-register'}}
@endsection

@section('content')

<div class="welcome-content">
{{-- {{dd($errors)}} --}}
    <div class="login-register-box">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Register</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Already registered? Login</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              {{-- <span class="invalid-feedback" role="alert">
                <strong> --}}
              @php
              echo '<script language="javascript">';
              @endphp
              @if (session()->has('login-message'))
                  {{-- <h5>{{ session('login-message') }}</h5> --}}
                  @php
                  echo 'alert("Wrong Credentials")';
                  @endphp
              @endif

              @php
              echo '</script>';
              @endphp

                @if (session()->has('auth-message'))
                    <h5>{{ session('auth-message') }}</h5>
                  @endif
                {{-- </strong>
              </span> --}}

                  @isset($url)
                  <form method="POST" id="form_registration" action='/register/auth/student' method="post" aria-label="{{ __('Register') }}" class="login-register-form form-register">
                  @else
                  <form method="POST" id="form_registration" action="/register/auth/student" method="post" aria-label="{{ __('Register') }}" class="login-register-form">
                  @endisset
                      @csrf
                    <div class="form-group field-name">
                        <input id="name" type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="{{ __('Name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group field-email">
                        <input id="email1" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('Email Address') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    {{-- <div class="form-group">
                        <input id="roll_number" type="roll_number" class="form-control{{ $errors->has('roll_number') ? ' is-invalid' : '' }}" name="roll_number" value="{{ old('roll_number') }}" placeholder="{{ __('Roll Number') }}" required>

                        @if ($errors->has('roll_number'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('roll_number') }}</strong>
                            </span>
                        @endif
                    </div> --}}
                    {{-- {{dd($errors)}} --}}
                    <div class="form-group field-phone">
                        <input id="phone" type="tel" name="phone_number" class="form-control" placeholder="{{ __('Mobile Number') }}" value="{{ old('phone_number') }}" required>
                        @if ($errors->has('phone_number'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone_number') }}</strong>
                            </span>
                        @endif
                        <input id="btn_otp" type="button" name="btn_otp" class="form-control" value="Verify">
                    </div>

                    <div class="form-group field-otp">
                        <input id="otp" type="text" name="otp" class="form-control" placeholder="Enter OTP" required>
                        {{-- @if ($errors->has('otp'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('otp') }}</strong>
                            </span>
                        @endif --}}
                    </div>

                    <div class="form-group field-password">
                        <input id="password1" type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group field-password">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{ __('Confirm Password') }}" required>
                    </div>

                    <div class="form-group field-state">
                      <select class="form-control" id="state" name="state" value="{{ old('state') }}" required>
                        <option value="">Select State</option>
                        <option value="Jammu & Kashmir">Jammu & Kashmir</option>
                        <option value="Uttarkhand">Uttarkhand</option>
                        <option value="Himachal Pradesh">Himachal Pradesh</option>
                        <option value="Haryana">Haryana</option>
                        <option value="Punjab">Punjab</option>
                        <option value="Rajasthan">Rajasthan</option>
                        <option value="Gujrat">Gujrat</option>
                        <option value="Uttar Pradesh">Uttar Pradesh</option>
                        <option value="Madhya Pradesh">Madhya Pradesh</option>
                        <option value="Bihar">Bihar</option>
                        <option value="West Bengal">West Bengal</option>
                        <option value="Sikkim">Sikkim</option>
                        <option value="Assam">Assam</option>
                        <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                        <option value="Manipur">Manipur</option>
                        <option value="Mizoram">Mizoram</option>
                        <option value="Nagaland">Nagaland</option>
                        <option value="Tripura">Tripura</option>
                        <option value="Meghalaya">Meghalaya</option>
                        <option value="Delhi">Delhi</option>
                        <option value="Maharashtra">Maharashtra</option>
                        <option value="Goa">Goa</option>
                        <option value="Odisa">Odisa</option>
                        <option value="Jharkhand">Jharkhand</option>
                        <option value="Andhra Pradesh">Andhra Pradesh</option>
                        <option value="Karnataka">Karnataka</option>
                        <option value="Tamil Nadu">Tamil Nadu</option>
                        <option value="Telangana">Telangana</option>
                        <option value="Kerala">Kerala</option>
                        <option value="Chattisgarh">Chattisgarh</option>
                      </select>
                    </div>

                    <div class="form-group field-course">
                      <select class="form-control" id="course" name="course"  value="{{ old('course') }}" required>
                        <option value="">Select Course</option>
                        <option value="B.com">B.com</option>
                        <option value="B.Tech">B.Tech</option>
                        <option value="BSC">BSC</option>
                      </select>
                    </div>

                    <button type="button" id="register_btn" class="btn btn-primary">
                        {{ __('Register') }}
                    </button>

                </form>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                @isset($url)
                  <form method="POST" action='{{ url("login/$url") }}' aria-label="{{ __('Login') }}" class="login-register-form form-login">
                  @else
                  <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" class="login-register-form form-login">
                  @endisset
                      @csrf

                     <div class="form-group field-email">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('Email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group field-password">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group field-checkbox">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>

                        @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}" class="forgot-link">
                            Forgot your password?
                        </a>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-primary">
                        {{ __('Login') }}
                    </button>

                </form>
            </div>
        </div>

    </div>
    <div class="made-by">
        Designed by Developed by
        <img src="{{ asset('images/logo-amakein.png') }}" alt="" />
    </div>

</div>
@endsection
@section('script')
  <script type="text/javascript">
    $('#btn_otp').on('click',function(){
      if (!$('#phone').val()) {
        alert('please enter the number');
        return;
      }
      if(!$('#phone').val().match(/^\d{10}$/))  {
          alert("Please put 10 digit mobile number");
          return;
      }
      // alert('hi')
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('input[name="_token"]').val()
          },
          method:'post',
          url: '/register/send-sms',
          data: {'phone_number':$('#phone').val()},
          success: function(data) {
              console.log(data);
              if (data=='1') {
                $('#otp').focus();
              }
              else{
                alert('please re-enter phone number');
              }
          },
          error: function(error) {
              console.log(error);
          }
      });
    })
    $('#register_btn').on('click',function(e){
      flag = 0;
      $.each($('#form_registration input,#form_registration select'),function(){
        if ($(this).val()=='') {
          $(this).addClass('error');
          flag = 1;
        }
      })
      if (flag == 1) {
        alert("Please fill all fields");
        return false;
      }
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('input[name="_token"]').val()
          },
          method:'get',
          url: '/register/verify-user',
          data: {'code':$('#otp').val()},
          success: function(data) {
              console.log(data);
              if (data=='verified') {
                console.log('done');
                $('#form_registration').submit();
                // window.location.href = "/new_student_profile_save_data";
              }
              else{
                alert('otp not verified');
              }
          },
          error: function(error) {
              console.log(error);
          }
      });
    });

    $(document).ready(function(){
        if($(window).width() < 640) {
            $("#profile-tab").text("Login");
        }
        else {
            $("#profile-tab").text("Already Registered? Login");
        }
    });
    $(window).resize(function(){
        if ($(window).width() <= 640) {
            $("#profile-tab").text("Login");
        }
        else {
            $("#profile-tab").text("Already Registered? Login");
        }
    });

  </script>
@endsection
