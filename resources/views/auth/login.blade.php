@extends('layouts.app')

@section('content')

<div class="login-box">
{{-- {{dd($errors)}} --}}
    <nav>
        <button type="button" name="login" id="login">
            Login
        </button>
        <button type="button" name="register" id="register">
            Register
        </button>
    </nav>
<!--    <div class="card-header">{{ __('Login') }}</div>-->

    <div class="login-register-form">
      @if (session()->has('login-message'))
          <h5>{{ session('login-message') }}</h5>
        @endif
          @isset($url)
          <form method="POST" action='{{ url("login/$url") }}' aria-label="{{ __('Login') }}">
          @else
          <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
          @endisset
              @csrf

            <div class="form-group">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('Email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn">
                    {{ __('Login') }}
                </button>

                @if (Route::has('password.request'))
                <p class="forgot-link">Forgot your password?
                    <a href="{{ route('password.request') }}">
                        {{ __('Click Here') }}
                    </a>
                </p>
                @endif
            </div>

        </form>
    </div>
</div>
@endsection
