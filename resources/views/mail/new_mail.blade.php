@extends('layouts.student')
  @section('content')
    <div class="content">
      New Message - <br>
        <a class="icon-previous" href="{{ URL::previous() }}" title="Previous"></a>
        <form class="" id="mail_data" action="/mail_sent" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="mail">
                <div class="form-group" id="TextBoxesGroup">
                    <label>{{ __('To') }}</label>
                    <input type="email" name="to" value="" class="form-control" required>
                </div>
                <div>
                <button type="button" name="button" id = "cc" >CC</button>
              </div>
                <div class="form-group">
                    <label>{{ __('Subject') }}</label>
                    <input type="text" name="subject" value="" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>{{ __('Body') }}</label>
                    <label for="file-upload" class="custom-file-upload">
                        <i class="fa fa-cloud-upload"></i>
                    </label>
                    <input id="file-upload" type="file"/>
                    <textarea name="mail_body" rows="8" cols="80" class="form-control" required></textarea>
                </div>
                <input type="submit" value="Send" >
                <button type="button" name="button">Save Draft</button>
            </div>
        </form>
    </div>
  @endsection
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script>
$(document).ready(function(){
  var counter = 2;
  $("#cc").click(function () {
    if(counter>2){
      // alert("Only 10 textboxes allow");
      return false;
    }
   var newTextBoxDiv = $(document.createElement('div'))
        .attr("id", 'TextBoxDiv' + counter);
   newTextBoxDiv.after().html('<br><label>{{ __('CC') }}</label>'+'<input type="email" name="cc' + counter +
         '" id="textbox' + counter + '" value="" class="form-control" required>');
   newTextBoxDiv.appendTo("#TextBoxesGroup");
   counter++;
   $(this).hide();
    });
  });
  </script>
