@extends('layouts.student')
@section('content')
    {{-- <a class="icon-previous" href="{{ URL::previous() }}" title="Previous"></a> --}}
    <h2 class="content-title">Exam Schedule</h2>
    @if ($datesheet)
      <ul class="info-list">
          @foreach ($datesheet as $key => $value)
              <li>
                  <label>{{$value->form_name}}</label>
                  <a target="_blank" href="{{$value->url}}">Click here for details</a>
              </li>
          @endforeach
      </ul>
    @else
      <p class="info-text">
          No information available.
          <span>Information will be updated here as soon as admin update it.</span>
      </p>
    @endif
@endsection
