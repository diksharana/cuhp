@extends('layouts.welcome_registration')
@section('body_classname')
{{'welcome-body student-register'}}
@endsection
@section('content')

<form class="student-registration-form" id="new_student_registration" action="send-sms" enctype="multipart/form-data">
  @csrf
    <div class="form-group no-icon">
        <label class="form-heading">
            Enter your details below for registration:<span>All fields are mandatory.</span>
        </label>
    </div>
    <div class="form-sub form-sub-no-bg">
        <div class="form-group field-course">
            <select name="course" class="form-control">
                <option value="">Select Course</option>
                <option value="Course 1">Course 1</option>
                <option value="Course 2">Course 2</option>
                <option value="Course 3">Course 3</option>
            </select>
        </div>
        <div class="form-group field-name">
            <input type="text" name="first_name" required value="" class="form-control" placeholder="First Name">
        </div>
        <div class="form-group field-name">
            <input type="text" name="last_name" value="" class="form-control" placeholder="Last Name">
        </div>
        <div class="form-group field-dob">
            <input type="date" name="DOB" required value="" class="form-control" placeholder="DD/MM/YYYY">
        </div>
        <div class="form-group form-group-check">
            <label>Gender</label>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" required name="gender" id="inlineRadio1" value="female">
                <label class="form-check-label" for="inlineRadio1">Female</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" required name="gender" id="inlineRadio2" value="male">
                <label class="form-check-label" for="inlineRadio2">Male</label>
            </div>
        </div>
    </div>
    <div class="form-sub">
        <div class="form-group form-subtitle">
            <label>Permanent Address</label>
        </div>
        <div class="form-group field-address">
            <input type="text" name="address_line1" required value="" class="form-control" placeholder="Address Line 1">
        </div>
        <div class="form-group field-address">
            <input type="text" name="address_line2" value="" class="form-control" placeholder="Address Line 2">
        </div>
        <div class="form-group field-city">
            <input type="text" name="city" value="" required class="form-control" placeholder="City">
        </div>
        <div class="form-group field-state">
            <input type="text" name="state" required value="" class="form-control" placeholder="State">
        </div>
        <div class="form-group field-postcode">
            <input type="text" name="code" required value="" class="form-control" placeholder="Postal Code">
        </div>
        <div class="form-group field-country">
            <select name="country" class="form-control" required>
                <option value="">Country</option>
                <option value="India">India</option>
                <option value="Sri Lanka">Sri Lanka</option>
                <option value="Afganistan">Afganistan</option>
            </select>
        </div>
    </div>
    <div class="form-sub form-correspondance">
        <div class="form-group form-subtitle">
            <label>Correspondence Address</label>
        </div>
        <div class="form-group form-group-check">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option1">
                <label class="form-check-label" for="inlineCheckbox3">Same as Permanent Address.</label>
            </div>
        </div>
        <div class="form-group field-address">
            <input type="text" name="address_line1_correspondance" value="" class="form-control" placeholder="Address Line 1">
        </div>
        <div class="form-group field-address">
            <input type="text" name="address_line2_correspondance" value="" class="form-control" placeholder="Address Line 2">
        </div>
        <div class="form-group field-city">
            <input type="text" name="city_correspondance" value="" class="form-control" placeholder="City">
        </div>
        <div class="form-group field-state">
            <input type="text" name="state_correspondance" value="" class="form-control" placeholder="State">
        </div>
        <div class="form-group field-postcode">
            <input type="text" name="code_correspondance" value="" class="form-control" placeholder="Postal Code">
        </div>
        <div class="form-group field-country">
            <select name="country_correspondance" class="form-control">
                <option value="">Country</option>
                <option value="India">India</option>
                <option value="Sri Lanka">Sri Lanka</option>
                <option value="Afganistan">Afganistan</option>
            </select>
        </div>
    </div>
    <div class="form-sub form-sub-no-bg">
        <div class="form-group field-email">
            <input type="email" name="email" value="" class="form-control" placeholder="Email">
        </div>
        <div class="form-group field-phone">
            <input type="tel" name="phone_number" value="" class="form-control" placeholder="Phone Number" required>
        </div>
        <div class="form-group field-name">
            <input type="text" name="mother_name" value="" class="form-control" placeholder="Mother's Name">
        </div>
        <div class="form-group field-name">
            <input type="text" name="father_name" value="" class="form-control" placeholder="Father's Name">
        </div>
        <div class="form-group field-phone">
            <input type="tel" name="parents_phone" value="" class="form-control" placeholder="Enter Phone of Parent">
        </div>
        <div class="form-group field-qualification">
            <select name="highest_qualification" class="form-control">
                <option value="">Highest Qualification</option>
                <option value="masters_degree">Masters degree</option>
                <option value="bachelors_degree">Bachelors degree</option>
                <option value="12th">12th</option>
                <option value="ITI">ITI</option>
                <option value="diploma">Diploma</option>
            </select>
        </div>
        <div class="form-group field-percent">
            <input type="text" name="percentage_highest_qualification" value="" class="form-control" placeholder="Percentage - Aggregate of Higher Education">
        </div>
        <div class="clearfix"></div>
<!--        <input type="submit" name="" value="Next" class="btn" >-->
        <button type="submit" class="btn btn-primary" id="new_user_data_save" data-toggle="modal" data-target="#exampleModal">
            Next
        </button>
        <p class="verify-message">
            After clicking the Next button, you will receive a code on mobile number you have entered. Please enter that code and proceed to next step.
        </p>
    </div>
</form>
<div class="made-by">
    Designed by Developed by
    <img src="{{ asset('images/logo-amakein.png') }}" alt="" />
</div>


@endsection
@section('script')
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $(".form-correspondance .form-group-check .form-check .form-check-input").on("click", function(){
          $(".form-correspondance .form-group").toggleClass("hide");
      });
      $('.datepicker').datepicker();
      $('form').on('submit',function(e){
        e.preventDefault();
      })
      $("#new_student_registration").on('submit',function(e){
        console.log(new FormData($('#new_student_registration')[0]))
        $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method:'post',
              url: '/new_student_profile_save_data',
              data: new FormData($('#new_student_registration')[0]),
              contentType: false,
              cache: false,
              processData: false,
              success: function(data) {
                  console.log("success");
              },
              error: function(data) {
                  console.log("error");
              }
          });
  });


    });

  </script>

@endsection
