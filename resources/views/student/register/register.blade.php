@php
  use \App\Http\Controllers\StudentController;
@endphp
@extends('layouts.welcome_registration')
@section('content')
<form id="detail" method="post" action="/student" class="registration-form" novalidate>
  @csrf
  <div id="msform">
    <ul id="progressbar">
      <li class="active">
          <span>Course Details</span>
      </li>
      <li>
          <span>Personal Information</span>
      </li>
      <li>
          <span>Academic Information</span>
      </li>
      <li>
          <span>Documentation</span>
      </li>
      <li>
          <span>Payment</span>
      </li>
    </ul>
    {{-- @foreach ($user_detail as $key => $value) --}}
      {{-- {{var_dump($user_detail->step).die()}} --}}
    <!-- Course Details -->
    <fieldset class="field-course-detials">
      <h2 class="fs-title">Select Course</h2>
       <div class="form-group">
          <label>Select Program Type *</label>
          <select  class="program form-control" name="program_type">
            {{-- <option selected="true" value={{$user_detail?$user_detail->program_type:'Select'}}>{{$user_detail?$user_detail->program_type:'Select'}}</option> --}}
            <option value="" selected disabled>select</option>
            <option value="Diploma" {{isset($user_detail) && $user_detail->program_type=='Diploma'?'selected':''}}>Diploma</option>
            <option value="Under-Graduation" {{isset($user_detail) && $user_detail->program_type=='Under-Graduation'?'selected':''}}>Under Graduation</option>
            <option value="Post-Graduation" {{isset($user_detail) && $user_detail->program_type=='Post-Graduation'?'selected':''}}>Post Graduation</option>
            <option value="Masters" {{isset($user_detail) && $user_detail->program_type=='Masters'?'selected':''}}>Masters</option>
            <option value="Ph.D" {{isset($user_detail) && $user_detail->program_type=='Ph.D'?'selected':''}}>Ph.D</option>
          </select>
      </div>
      <div class="form-group">
          @php
            $courses = ['BTech','BCom','BA'];
          @endphp
          <label>Select Course</label>
          <select class="program form-control" name="course">
            <option value="" selected disabled>select</option>
            @foreach ($courses as $key => $course)
              <option value="{{$course}}" {{isset($user_detail) && $user_detail->course==$course?'selected':''}}>{{$course}}</option>
            @endforeach
          </select>
      </div>
      <div class="form-group">
          @php
            $branches = ['CSE','Civil','Mechanical'];
          @endphp
          <label>Select Branch/Subject</label>
          <select class="program form-control" name="subject">
            <option value="" selected disabled>select</option>
            @foreach ($branches as $key => $branch)
              <option value="{{$branch}}" {{isset($user_detail) && $user_detail->subject==$branch?'selected':''}}>{{$branch}}</option>
            @endforeach
          </select>
      </div>
      <div class="form-group">
          @php
            $faculties = ['John','Peter','Maxwell'];
          @endphp
          <label>Select Faculty</label>
          <select  class="program form-control" name="faculty">
            <option value="" selected disabled>select</option>
            @foreach ($faculties as $key => $faculty)
              <option value="{{$faculty}}" {{isset($user_detail) && $user_detail->faculty==$faculty?'selected':''}}>{{$faculty}}</option>
            @endforeach
          </select>
      </div>
      <div class="clearfix"></div>
      <input type="button" id="step1" name="next" class="btn next action-button" value="Next" />
    </fieldset>

    <!-- Personal Details -->
    <fieldset class="field-person-detials">
        <h2 class="fs-title">Personal Details</h2>
        <div class="form-group">
            <label>First Name</label>
              <input type="text" name="first_name" value="{{$user_detail?$user_detail->first_name:''}}" class="form-control detail" placeholder="First Name">
        </div>
        <div class="form-group">
            <label>Middle Name</label>
            <input type="text" name="middle_name" value="{{$user_detail?$user_detail->middle_name:''}}" class="form-control" placeholder="Middle Name">
        </div>
        <div class="form-group">
            <label>Last Name</label>
            <input type="text" name="last_name" value="{{$user_detail?$user_detail->last_name:''}}" class="form-control detail" placeholder="Last Name">
          </div>
        <div class="form-group">
            <label>Date of Birth</label>
            <input type="date" name="DOB" value="{{$user_detail?$user_detail->DOB:''}}" class="form-control detail" placeholder="yyyy/mm/dd">
        </div>
        <div class="form-group form-group-full">
            <label>Gender</label>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="female" {{isset($user_detail) && $user_detail->gender=='female'? 'checked':''}}>
                <label class="form-check-label" for="inlineRadio1">Female</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="male" {{isset($user_detail) && $user_detail->gender=='male'? 'checked':''}}>
                <label class="form-check-label" for="inlineRadio2">Male</label>
            </div>
        </div>
         <div class="form-group form-sub">
            <label class="form-subtitle">Permanent Address</label>
        </div>
        <div class="form-permanent">
            <div class="form-group">
                <label>Address Line 1</label>
                  <input type="text" id="address_line1" name="address_line1" value="{{$user_detail?$user_detail->address_line1:''}}" class="form-control detail" placeholder="Address Line 1">
            </div>
            <div class="form-group">
                <label>Address Line 2</label>
                  <input type="text" id="address_line2" name="address_line2" value="{{$user_detail?$user_detail->address_line2:''}}" class="form-control detail" placeholder="Address Line 2">
            </div>
            <div class="form-group">
                <label>City</label>
                  <input type="text" id="city" name="city" value="{{$user_detail?$user_detail->city:''}}" class="form-control detail" placeholder="City">
            </div>
            <div class="form-group">
                <label>State</label>
                  <input type="text" id="state" name="state" value="{{$user_detail?$user_detail->state:''}}" class="form-control detail" placeholder="State">
            </div>
            <div class="form-group">
                @php
                  $countries = ['India','Sri Lanka','Afganistan'];
                @endphp
                <label>Country</label>
                <select id="country" name="country" class="form-control detail">
                  <option value="" selected disabled>select</option>
                  @foreach ($countries as $country)
                    <option value="{{$country}}" {{isset($user_detail) && $user_detail->country==$country?'selected':''}}>{{$country}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Postal Code</label>
                <input type="text" id="code" name="code" value="{{$user_detail?$user_detail->code:''}}" class="form-control detail" placeholder="Enter Postal Code">
            </div>
      </div>
        <div class="form-group form-sub">
            <label class="form-subtitle">Correspondence Address</label>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option1">
                <label class="form-check-label" for="inlineCheckbox3">Same as Permanent Address.</label>
            </div>
        </div>
        <div class="form-corresspondane">
            <div class="form-group">
                <label>Address Line 1</label>
                  <input type="text" id="address_line1_correspondance" name="address_line1_correspondance" value="{{$user_detail?$user_detail->address_line1_correspondance:''}}" class="form-control detail" placeholder="Address Line 1">
            </div>
            <div class="form-group">
                <label>Address Line 2</label>
                  <input type="text" id="address_line2_correspondance" name="address_line2_correspondance" value="{{$user_detail?$user_detail->address_line2_correspondance:''}}" class="form-control detail" placeholder="Address Line 2">
              </div>
            <div class="form-group">
                <label>City</label>
                  <input type="text" id="city_correspondance" name="city_correspondance" value="{{$user_detail?$user_detail->city_correspondance:''}}" class="form-control detail" placeholder="City">
            </div>
            <div class="form-group">
                <label>State</label>
                  <input type="text" id="state_correspondance" name="state_correspondance" value="{{$user_detail?$user_detail->state_correspondance:''}}" class="form-control detail" placeholder="State">
            </div>
            <div class="form-group">
                @php
                  $countries = ['India','Sri Lanka','Afganistan'];
                @endphp
                <label>Country</label>
                <select id="country_correspondance" name="country_correspondance" class="form-control detail">
                    <option selected disabled>select</option>
                    @foreach ($countries as $country)
                      <option value="{{$country}}" {{isset($user_detail) && $user_detail->country_correspondance==$country?'selected':''}}>{{$country}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Postal Code</label>
                  <input type="text" id="code_correspondance" name="code_correspondance" value="{{$user_detail?$user_detail->code_correspondance:''}}" class="form-control detail" placeholder="Postal Code">
            </div>
        </div>

        <div class="form-group">
            <label>Email Address</label>
              <input type="text" id="email" name="email" value="{{$user_detail?$user_detail->email:''}}" class="form-control detail" placeholder="Email">
        </div>
        <div class="form-group">
            <label>Phone Number</label>
              <input type="text" id="phone_number" name="phone_number" value="{{$user_detail?$user_detail->phone_number:''}}" class="form-control detail" placeholder="Phone Number">
        </div>
        <div class="form-group">
            <label>Mother's Name</label>
              <input type="text" id="mother_name" name="mother_name" value="{{$user_detail?$user_detail->mother_name:''}}" class="form-control detail" placeholder="Mother's Name">
        </div>
        <div class="form-group">
            <label>Father's Name</label>
              <input type="text" id="father_name" name="father_name" value="{{$user_detail?$user_detail->father_name:''}}" class="form-control detail" placeholder="Father's Name">
        </div>
        <div class="form-group float-left">
            <label>Parent Phone Number</label>
              <input type="tel" id="parents_phone" name="parents_phone" value="{{$user_detail?$user_detail->parents_phone:''}}" class="form-control detail" placeholder="Enter Phone of Parent">
        </div>
        <div class="clearfix"></div>
        <input type="button" name="previous" class="btn previous action-button" value="Previous" />
        <input type="button" id="step2" name="next" class="btn next2 action-button" value="Next" />
    </fieldset>

    <!-- Academic Details -->
    <fieldset class="field-academic-information">
        <h2 class="fs-title">Academic Information</h2>
        <div class="form-group">
          @php
            $highest_qualifications = ['Diploma','Under-Graduation','Post-Graduation'];
          @endphp
          <label>Select Highest Qualification Course</label>
          <select class="form-control form-3" id="highest_qualification" name="highest_qualification">
            <option selected disabled>select</option>
            @foreach ($highest_qualifications as $key => $highest_qualification)
              <option value="{{$highest_qualification}}" {{isset($user_detail) && $user_detail->highest_qualification==$highest_qualification?'selected':''}}>{{$highest_qualification}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
            <label>Enter Percentage</label>
            <input class="form-3 form-control" type="text" id="percentage" name="percentage_highest_qualification" value="{{$user_detail?$user_detail->percentage_highest_qualification:''}}" placeholder="Percentage - Aggregate of Higher Education">
        </div>
        <div class="clearfix"></div>
        <input type="button" name="previous" class="btn previous action-button" value="Previous" />
       <input type="button" id="step3" name="next" class="btn next3 action-button" value="Next" />
    </fieldset>

    <!-- Document Details -->
    <fieldset class="field-document document">
      <h2 class="fs-title">Documents</h2>
      <div class="form-group form-group-full">
          <label class="form-heading">Upload your documents for verification: </label><br/>
          <label><span>Note :</span> Photo dimensions - (360 * 240) . <br/>Signature should be in black color on white paper. <br/>Photo uploaded here will reflect on your profile page and admit card.</label>
      </div>
      <div class="form-group">
          <label>10th Marksheet</label>
          <span>{{$user_detail?StudentController::lastSlashString($user_detail->marksheet_10th):''}}</span>
          {{-- @if (isset($user_detail) && $user_detail->marksheet_10th)
            <a href="{{asset($user_detail->marksheet_10th)}}">{{asset($user_detail->marksheet_10th)}}</a>
          @endif --}}
          <input type="file" name="marksheet_10th" value="{{$user_detail?$user_detail->marksheet_10th:''}}" class="form-control-file">
      </div>
      <div class="form-group">
          <label>12th Marksheet</label>
          <span>{{$user_detail?StudentController::lastSlashString($user_detail->marksheet_12th):''}}</span>
          {{-- @if (isset($user_detail) && $user_detail->marksheet_12th)
            <a href="{{asset($user_detail->marksheet_12th)}}">{{asset($user_detail->marksheet_12th)}}</a>
          @endif --}}
          <input type="file" name="marksheet_12th" value="{{$user_detail?$user_detail->marksheet_12th:''}}" class="form-control-file">
      </div>
      <div class="form-group">
          <label>Adhar Card</label>
          <span>{{$user_detail?StudentController::lastSlashString($user_detail->adhar_card):''}}</span>
          {{-- @if (isset($user_detail) && $user_detail->adhar_card)
            <a href="{{asset($user_detail->adhar_card)}}">{{asset($user_detail->adhar_card)}}</a>
          @endif --}}
          <input type="file" name="adhar_card" value="{{$user_detail?$user_detail->adhar_card:''}}" class="form-control-file">
      </div>
      <div class="form-group">
          <label>Photo</label>
          <span>{{$user_detail?StudentController::lastSlashString($user_detail->photo):''}}</span>
          {{-- @if (isset($user_detail) && $user_detail->photo)
            <a href="{{asset($user_detail->photo)}}">{{asset($user_detail->photo)}}</a>
          @endif --}}
          <input type="file" name="photo" value="{{$user_detail?$user_detail->photo:''}}" class="form-control-file">
      </div>
      <div class="form-group">
          <label>Signature</label>
          <span>{{$user_detail?StudentController::lastSlashString($user_detail->signature):''}}</span>
          {{-- @if (isset($user_detail) && $user_detail->signature)
            <a href="{{asset($user_detail->signature)}}">{{asset($user_detail->signature)}}</a>
          @endif --}}
          <input type="file" name="signature" value="{{$user_detail?$user_detail->signature:''}}" class="form-control-file">
      </div>
      <div class="clearfix"></div>
      <input type="button" name="previous" class="btn previous action-button" value="Previous" />
      <input type="button" id="step4" name="next" class="btn next4 action-button" value="Next" />
    </fieldset>
    <!-- Payment Details -->
    <fieldset class="field-payment">
      <div class="loader"></div>
        {{-- <h2 class="fs-title">Payment</h2> --}}
        {{-- <div class="form-group">
            <label>First Name</label>
            <input type="text" name="fname" placeholder="First Name" class="form-control" />
        </div>
        <div class="form-group">
            <label>Last Name</label>
            <input type="text" name="lname" placeholder="Last Name" class="form-control" />
        </div>
        <div class="form-group">
            <label>Card Number</label>
            <input type="text" name="phone" placeholder="Phone" class="form-control" />
        </div>
        <div class="clearfix"></div> --}}
        <div class="final_confirmation">
            <h2>Final Step</h2>
            {{-- @if ($user_detail && $user_detail->declaration == 'checked')
            @else --}}
              <label>Your application have been submitted successfully and is under consideration.</label>
              <p>Before moving on to the final step for payment.</p>
              <label>
                  <input type="checkbox" id="checkbox_declaration" name="declaration" class="form-control" value="checked" {{$user_detail?$user_detail->declaration:''}}/>
                  Please confirm all the information entered by you is as per the application and correct.
              </label>
            {{-- @endif --}}
        </div>

        <input type="hidden" id="step" name="step" value="{{$user_detail?$user_detail->step:1}}">
          <input type="button" name="previous" id="previous" class="btn previous action-button" value="Previous" />
        <input type="submit" id="step5" name="submit" class="btn submit action-button" value="Submit" />
    </fieldset>
  </div>
</form>

@endsection

@section('script')

  <script type="text/javascript">
    $(document).ready(function(){
      $('#step5').attr("disabled", true);
      $(".form-check-input").on("click", function(){
          // $(".form-correspondance .form-group").toggleClass("hide");
          if (this.checked) {
            $("#address_line1_correspondance").val($("#address_line1").val());
            $("#address_line2_correspondance").val($("#address_line2").val());
            $("#city_correspondance").val($("#city").val());
            $("#state_correspondance").val($("#state").val());
            $("#country_correspondance").val($("#country option:checked").val());
            $("#code_correspondance").val($("#code").val());
//            $(".form-sub .form-check-input").click(function(){
//                $(".form-corresspondane").toggleClass("hide");
//            });
          }
          // else {
          //   $("#address_line1").val('');
          //   $("#address_line2").val('');
          //   $("#city").val('');
          //   $("#state").val('');
          //   $("#country").val('');
          //   $("#code").val('');
          // }
      });
      $('.datepicker').datepicker();
      // $('form').on('submit',function(e){
      //   e.preventDefault();
      // })
      $(".form-sub .form-check-input").click(function(){
          $(".form-corresspondane").toggleClass("hide");
      });
      $(".btn next action-button").click(function(){
        var empty = true;
          $('input[type="text"]').each(function(){
             if($(this).val()!=""){
                empty =false;
                alert('no');
                return false;
              }
          });
          if( !$('#program').val() ) {
            alert('no option selected');
          }
      })
      $('#step5').on('click', function(){
        if ($('input[name="declaration"]').is(':checked')) {
          $('#previous').prop('hidden', true);
          alert('application submitted successfully');
          //window.location.href="/student";
        }
        else{
          alert('please confirm the declaration');
          return false;
        }
      });
      $('input[name="declaration"]').on('click',function(e){
        if ($('input[name="declaration"]').is(':checked')) {
          $('#step5').attr("disabled", false);;
        }
        else{
          $('#step5').attr("disabled", true);;
        }
      })
      if ($('input[name="declaration"]').is(':checked')) {
        $('#step5').attr("disabled", false);;
      }
    });

  (function($) {
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches
    next_fs = $('#step'+{{$user_detail?$user_detail->step:1}}).parent();
    next_fx_siblings = $('#step'+{{$user_detail?$user_detail->step:1}}).parent().siblings().not('ul');
    next_fx_siblings.hide();
    $("#progressbar li:first").nextUntil($("#progressbar li:eq({{$user_detail?$user_detail->step:1}})")).addClass("active");
    next_fs.show();
    $(".next").click(function(e) {
      flag=0;
      $.each($('.program'),function(i,v){
        if (!$(this).val()) {
          $(this).addClass('error');
          flag=1;
          e.preventDefault();
        }
      })
      if (flag) {
        alert('Some required fields are empty.')
      }
      else {
        display($(this));
      }
    });
    $('#step2').click(function(e){
      flag = 0;
      $('.detail').each(function(){
        // console.log($(this).prop('required'));
        if(!$(this).val()){
          $(this).addClass('error');
          flag = 1;
        }
      })
      if(!$('input:radio').is(':checked')) {
        $(this).addClass('error');
        flag = 1;
      }
      // console.log(flag);
        if (flag) {
          alert('Some required fields are empty.')
            flag=0;
            return false;
        }
        else {
          display($(this));
        }
    })
    $('#step3').click(function(e) {
      flag = 0;
      $('.form-3').each(function(){
        if(!$(this).val()){
          $(this).addClass('error');
          flag = 1;
        }
      })
      if(flag) {
        alert('Enter Details');
        return false;
      }
      else {
        display($(this));
      }
    })
    $('#step4').click(function(e){
      flag=0
      $.each($('input[type=file]'),function(i,v) {
        if (!$(this).attr('value') && $(this).get(0).files.length === 0) {
          $(this).addClass('error');
          $(this).addClass('error');
          flag=1;
        }
      });
      if (flag) {
        alert('Some required fields are empty.')
      }
      else {
        display($(this));
      }
    })
    $(".previous").click(function() {
      if (animating) return false;
      animating = true;
      current_fs = $(this).parent();
      previous_fs = $(this).parent().prev();
      //de-activate current step on progressbar
      $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
      //show the previous fieldset
      previous_fs.show();
      //hide the current fieldset with style
      current_fs.animate({
        opacity: 0
      }, {
        step: function(now, mx) {
          //as the opacity of current_fs reduces to 0 - stored in "now"
          //1. scale previous_fs from 80% to 100%
          scale = 0.8 + (1 - now) * 0.2;
          //2. take current_fs to the right(50%) - from 0%
          left = ((1 - now) * 50) + "%";
          //3. increase opacity of previous_fs to 1 as it moves in
          opacity = 1 - now;
          current_fs.css({
            'left': left
          });
          previous_fs.removeClass("hide");
          previous_fs.css({
            'transform': 'scale(' + scale + ')',
            'opacity': opacity
          });
        },
        duration: 800,
        complete: function() {
          current_fs.hide();
          animating = false;
        },
        // //this comes from the custom easing plugin
        // easing: 'easeInOutBack'
      });
      $('#step').val( function(i, oldval) {
          return --oldval;
      });
      save_data();
    });
    function display(x){
      $('.loader').show();
      flag=0;
      if (animating) return false;
      animating = true;

      current_fs = x.parent();
      next_fs = x.parent().next();

      //activate next step on progressbar using the index of next_fs
      $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

      //show the next fieldset
      next_fs.show();
      //hide the current fieldset with style
      current_fs.animate({
        opacity: 0
      }, {
        step: function(now, mx) {
          //as the opacity of current_fs reduces to 0 - stored in "now"
          //1. scale current_fs down to 80%
          scale = 1 - (1 - now) * 0.2;
          //2. bring next_fs from the right(50%)
          left = (now * 50) + "%";
          //3. increase opacity of next_fs to 1 as it moves in
          opacity = 1 - now;
          current_fs.addClass("hide");
          current_fs.css({
            'transform': 'scale(' + scale + ')'
          });
          next_fs.css({
            'left': left,
            'opacity': opacity
          });
        },
        duration: 800,
        complete: function() {
          current_fs.hide();
          animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
      })
      save_data();
      $('#step').val( function(i, oldval) {
        return oldval>4?oldval:++oldval;
      });
    }
    $('select').on('change',function(){
      if ($(this).hasClass('error')) {
        $(this).removeClass('error');
      }
    })
    $('input').on('focus',function(){
      if ($(this).hasClass('error')) {
        $(this).removeClass('error');
      }
    })
    function save_data(){
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('input[name="_token"]').val()
          },
          method:'post',
          url: '/register/detail',
          data: new FormData($('#detail')[0]),
          contentType: false,
          cache: false,
          processData: false,
          success: function(data) {
              console.log(data);
              $('.loader').hide();
          },
          error: function(error) {
              console.log(error);
              $('.loader').hide();
          }
      });
    }
  })(jQuery);
  //     $("#new_student_registration").on('submit',function(e){
  //       console.log(new FormData($('#new_student_registration')[0]))
  //       $.ajax({
  //             headers: {
  //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //             },
  //             method:'post',
  //             url: '/new_student_profile_save_data',
  //             data: new FormData($('#new_student_registration')[0]),
  //             contentType: false,
  //             cache: false,
  //             processData: false,
  //             success: function(data) {
  //                 console.log("success");
  //             },
  //             error: function(data) {
  //                 console.log("error");
  //             }
  //         });
</script>
@endsection
