@extends('layouts.welcome_registration')

@section('content')
<h2 class="content-title">Application Forms</h2>
{{-- @foreach ($attendance as $key => $value) --}}
<div id="content" class="application-form">
      {{-- <h2 class="admin-page-title">Application List</h2> --}}
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Application Form	</th>
              <th>Application No.	</th>
              <th>Application Submitted On	</th>
              <th>Application Fees	</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {{-- @foreach($users as $sql) --}}
            <tr id="">
              <td> Registration Form </td>
              <td> 1234 </td>
              <td>
                @if($data[0]->status=='registered')
                    {{'Application not submitted'}}
                @else
                  {{$data[0]->updated_at}}
                @endif
              </td>
              <td> Rs 500 </td>
              <td> @if ($data[0]->status=='registered')
                    <a href="/register/student" class="btn">Apply</a>
                  @elseif ($data[0]->status=='applied')
                    <p>Application Submitted.</p>
                    <a href="/view_application" class="btn">View Application</a>
                  @elseif ($data[0]->status=='approved')
                    <p>Application Approved.</p>
                    <a href="/view_application" class="btn">View Application</a>
                  @elseif ($data[0]->status=='rejected')
                    <p>Application Rejected.</p>
                    <p>Reason : {{$data[0]->reason}}</p>
                    <a href="/register/student" class="btn">Edit Application</a>
                    {{-- <div class="">
                      <form class="" action="/student/payment" method="post">
                        @csrf
                        <input type="submit" name="pay" class="btn" value="pay fee">
                      </form>
                    </div> --}}
                  @endif

                {{-- <button type="button" name="button" class="view btn">View</button>
                @if($sql->status=='Published')
                  <button type="button" name="button" class="approve btn">Approve</button>
                  <button type="button" name="button" id="delete" class="delete btn">Reject</button>
                @endif --}}
              </td>
            </tr>
            {{-- @endforeach --}}
          </tbody>
        </table>
      </div>
</div>

  {{-- @endforeach --}}


@endsection
