@extends('layouts.student')

@section('content')
<a class="icon-previous" href="{{ URL::previous() }}" title="Previous"></a>
<h2 class="content-title"> Timetable </h2>
@foreach ($timetable as $key => $value)
    <a target="_blank" href="{{$value->url}}">{{$value->form_name}}</a>
@endforeach
<ul class="list-box">
    <li>
        <a href="">Time-Table: Monsoon Semester 2014</a>
    </li>
    <li>
        <a href="">Time Table : Spring Semester, 2014</a>
    </li>
    <li>
        <a href="">Time-Table: Monsoon Semester 2013</a>
    </li>
</ul>
@endsection
