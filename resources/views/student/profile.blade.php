@extends('layouts.student')

@section('content')


<!--<a class="icon-previous" href="{{ URL::previous() }}" title="Previous"></a>-->
<h2 class="content-title">Profile</h2>
<form class="" id="profile_data" action="" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  @foreach ($data as $value)
    <div class="profile-left">
        {{-- @if ($value->avatar=='sample.jpg') --}}
            {{-- <span class="sample-img"> --}}
                <img src="{{$profile_image?asset($profile_image):asset('images/icons/icon-profile.png')}}" alt="{{asset('images/icons/icon-profile.png')}}">
            {{-- </span> --}}
          {{-- @else
            <span>
                <img src="{{$value->avatar}}" width="100px" height="100px" alt="{{$value->avatar}}">
            </span>
        @endif --}}
    </div>
    <div class="profile-right">
        <div class="form-group">
            <input type="text" id="studentName" name="name" value="{{$value->name}}" class="form-control" required readonly>
            <label for="studentName">{{ __('Name') }}</label>
        </div>

        <div class="form-group">
            <input type="text" id="studentEmail" name="email" value="{{$value->email}}" class="form-control" required readonly>
            <label for="studentEmail">{{ __('Email Address') }}</label>
        </div>

        {{-- <div class="form-group">
            <input type="text" id="studentRollNumber" name="roll_number" value="{{$value->roll_number}}" class="form-control" required>
            <label for="studentRollNumber">{{ __('Roll Number') }}</label>
        </div> --}}

        <div class="form-group">
            @if ($value->phone_number == 'default')
                <input type="tel" name="phone_number" value="" placeholder="Enter your Mobile Number" class="form-control" required readonly>
                @else
                <input type="tel" id="studentPhoneNumber" name="phone_number"  value="{{$value->phone_number}}" class="form-control" required readonly>
            @endif
            <label for="studentPhoneNumber">{{ __('Phone Number') }}</label>
        </div>

        <div class="form-group">
            @if ($value->course=='course')
              <input type="text" id="studentCourse" name="course" value="" placeholder="Enter your Course" class="form-control" required readonly>
              @else
              <input type="text" id="studentCourse" name="course"  value="{{$value->course}}" class="form-control" required readonly>
            @endif
            <label for="studentCourse">{{ __('Course') }}</label>
        </div>

        <div class="form-group">
            @if ($value->state=='state')
              <input type="text" id="studentState" name="state_name" value="" placeholder="Enter your State Name" class="form-control" required readonly>
              @else
              <input type="text" id="studentState" name="state_name"  value="{{$value->state}}" class="form-control" required readonly>
            @endif
            <label for="studentState">{{ __('State') }}</label>
        </div>

        {{-- <div class="form-group">
            @if ($value->mother_name=='Mother')
              <input type="text" id="studentMother" name="mother_name" value="" placeholder="Enter your Mother Name" class="form-control" required>
              @else
              <input type="text" id="studentMother" name="mother_name"  value="{{$value->mother_name}}" class="form-control" required>
            @endif
            <label for="studentMother">{{ __('Mother Name') }}</label>
        </div>

        <div class="form-group">
            @if ($value->parents_phone=='Phone')
              <input type="tel" id="studentParentNumber" name="parents_phone" value="" placeholder="Enter your Parents Phone" class="form-control" required>
              @else
              <input type="tel" id="studentParentNumber" name="parents_phone"  value="{{$value->parents_phone}}" class="form-control" required>
            @endif
            <label for="studentParentNumber">{{ __('Parents Phone') }}</label>
        </div>

        <div class="form-group">
            @if ($value->reg_no=='Regno')
              <input type="text" id="studentRegistration" name="reg_no" value="" placeholder="Enter your Registration Number" class="form-control" required>
              @else
              <input type="text" id="studentRegistration" name="reg_no"  value="{{$value->reg_no}}" class="form-control" required>
            @endif
            <label for="studentRegistration">{{ __('Registration Number') }}</label>
        </div>

        <div class="form-group">
            @if ($value->semester=='semester')
              <input type="text" id="studentSemester" name="semester" value=""  placeholder="Enter your Semester" class="form-control" required>
              @else
              <input type="text" id="studentSemester" name="semester"  value="{{$value->semester}}" class="form-control" required>
            @endif
            <label for="studentSemester">{{ __('Semester') }}</label>
        </div>

        <div class="form-group">
            @if ($value->address=='default')
              <input type="text" id="studentAddress" name="address" value=""  placeholder="Enter your Address" class="form-control" required>
              @else
              <input type="text" id="studentAddress" name="address"  value="{{$value->address}}" class="form-control" required>
            @endif
            <label for="studentAddress">{{ __('Address') }}</label>
        </div>

        {{-- {{dd($value->DOB)}}
        <div class="form-group">
            @if ($value->DOB=='NULL')
              <input type="date" id="sudentDOB" placeholder="Enter your DOB" class="form-control" required>
              @else
              <input type="date" id="sudentDOB" name="DOB"  value="{{$value->DOB}}" class="form-control" required>
            @endif
            <label for="sudentDOB">{{ __('Date of Birth') }}</label>
        </div>

        <input type="hidden" name="table" value="Student">
        <input type="submit" name="" id="save" value="Update" class="btn"> --}}
        {{-- @if (Auth::user()->status=='registered')
          <button type="button" class="btn" onclick="location.href = '/register/student'" name="button">Continue</button>
            @elseif (Auth::user()->status=='applied')

            @endif --}}

    </div>
  @endforeach
</form>
@endsection
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#profile_data').on('submit',function(e) {
    e.preventDefault();
    console.log(new FormData($('#profile_data')[0]));
    axios({
      method: 'post',
      url: '/profile_save_data',
      data: new FormData($('#profile_data')[0]),
      contentType: false,
      cache: false,
      processData: false,
      config: { headers: {'Content-Type': 'multipart/form-data' }}
    })
    .then(function (response) {

        location.reload();
    })
    .catch(function (response) {
        console.log(response);
    });
  });
});
</script> --}}
