@extends('layouts.student')

@section('content')
    {{-- <a class="icon-previous" href="{{ URL::previous() }}" title="Previous"></a> --}}
    <h2 class="content-title">Admit Card</h2>
    @if (count($courses))
      <ul class="info-list">
          @foreach ($courses as $key => $value)
            <li>
                <label>{{'Admit card : ' . $value->date_of_examination}}</label>
                <a target="_blank" href="{{url('/generate-pdf')}}">Click here to download admit card</a>
            </li>
          @endforeach
      </ul>
    @else
      <p class="info-text">
          No information available.
          <span>Information will be updated here as soon as admin update it.</span>
      </p>
    @endif
@endsection
