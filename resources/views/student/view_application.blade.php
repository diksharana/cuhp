@extends('layouts.welcome_registration')
@section('content')
<div class="view-application">
    <a href="{{ url()->previous() }}" class="btn pull-right">Go Back</a>
    @if (isset($message))
      <h2>{{$message}}</h2>
    @else
    <h2>Application Submitted</h2>
    <div class="field-group">
        <h3>Selected Course</h3>
        <div class="field-info">
            <label>Selected Program Type</label>
            <span>{{$student->program_type}}</span>
        </div>
        <div class="field-info">
            <label>Selected Faculty</label>
            <span>{{$student->faculty}}</span>
        </div>
        <div class="field-info">
            <label>Selected Course</label>
            <span>{{$student->course}}</span>
        </div>
        <div class="field-info">
            <label>Selected Branch/Subject</label>
            <span>{{$student->subject}}</span>
        </div>
    </div>


    <div class="field-group">
        <h3>Personal Details</h3>
        <div class="field-info">
            <label>First Name</label>
            <span>{{$student->first_name}}</span>
        </div>
        <div class="field-info">
            <label>Middle Name</label>
            <span>{{$student->middle_name}}</span>
        </div>
        <div class="field-info">
            <label>Last Name</label>
            <span>{{$student->last_name}}</span>
        </div>
        <div class="field-info">
            <label>Date of Birth</label>
            <span>{{$student->DOB}}</span>
        </div>
        <div class="field-info">
            <label>Gender</label>
            <span>{{$student->gender}}</span>
        </div>
        <div class="field-info no-field-bg">
            <label class="form-subtitle">Permanent Address</label>
        </div>
        <div class="form-permanent">
            <div class="field-info">
                <label>Address Line 1</label>
                <span>{{$student->address_line1}}</span>
            </div>
            <div class="field-info">
                <label>Address Line 2</label>
                <span>{{$student->address_line2}}</span>
            </div>
            <div class="field-info">
                <label>City</label>
                <span>{{$student->city}}</span>
            </div>
            <div class="field-info">
                <label>State</label>
                <span>{{$student->state}}</span>
            </div>
            <div class="field-info">
                <label>Country</label>
                <span>{{$student->country}}</span>
            </div>
            <div class="field-info">
                <label>Postal Code</label>
                <span>{{$student->code}}</span>
            </div>

        </div>
        <div class="field-info no-field-bg">
            <label class="form-subtitle">Correspondence Address</label>
        </div>
        <div class="form-corresspondane">
            <div class="field-info">
                <label>Address Line 1</label>
                <span>{{$student->address_line1_correspondance}}</span>
            </div>
            <div class="field-info">
                <label>Address Line 2</label>
                <span>{{$student->address_line2_correspondance}}</span>
            </div>
            <div class="field-info">
                <label>City</label>
                <span>{{$student->city_correspondance}}</span>
            </div>
            <div class="field-info">
                <label>State</label>
                <span>{{$student->state_correspondance}}</span>
            </div>
            <div class="field-info">
                <label>Postal Code</label>
                <span>{{$student->code_correspondance}}</span>
            </div>
            <div class="field-info">
                <label>Country</label>
                <span>{{$student->country_correspondance}}</span>
            </div>
        </div>
        <div class="field-info">
            <label>Email Address</label>
            <span>{{$student->email}}</span>
        </div>
        <div class="field-info">
            <label>Phone Number</label>
            <span>{{$student->phone_number}}</span>
        </div>
        <div class="field-info">
            <label>Mother's Name</label>
            <span>{{$student->mother_name}}</span>
        </div>
        <div class="field-info">
            <label>Father's Name</label>
            <span>{{$student->father_name}}</span>
        </div>
        <div class="field-info">
            <label>Parent Phone Number</label>
            <span>{{$student->parents_phone}}</span>
        </div>
    </div>


    <div class="field-group">
        <h3>Academic Information</h3>
        <div class="field-info">
            <label>Select Highest Qualification Course</label>
            <span>{{$student->highest_qualification}}</span>
        </div>
        <div class="field-info">
            <label>Enter Percentage</label>
            <span>{{$student->percentage_highest_qualification}}</span>
        </div>
    </div>


    <div class="field-group">
        <h3>Documentation</h3>
        <div class="field-info">
            <label>10th Marksheet</label>
            <span><a href="{{asset($student->marksheet_10th)}}" target="_blank">{{asset($student->marksheet_10th)}}</a></span>
        </div>
        <div class="field-info">
            <label>12th Marksheet</label>
            <span><a href="{{asset($student->marksheet_12th)}}" target="_blank">{{asset($student->marksheet_12th)}}</a></span>
        </div>
        <div class="field-info">
            <label>Adhar Card</label>
            <span><a href="{{asset($student->adhar_card)}}" target="_blank">{{asset($student->adhar_card)}}</a></span>
        </div>
        <div class="field-info">
            <label>Photo</label>
            <span><a href="{{asset($student->photo)}}" target="_blank">{{asset($student->photo)}}</a></span>
        </div>
        <div class="field-info">
            <label>Signature</label>
            <span><a href="{{asset($student->signature)}}" target="_blank">{{asset($student->signature)}}</a></span>
        </div>
    </div>
    <a href="{{ url()->previous() }}" class="btn">Back</a>
  @endif
</div>
@endsection
