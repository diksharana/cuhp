@extends('layouts.student')

@section('content')
<a class="icon-previous" href="{{ URL::previous() }}" title="Previous"></a>
<h2 class="content-title">Others</h2>
@foreach ($forms as $key => $value)
  <a target="_blank" href="{{$value->url}}">{{$value->form_name}}</a>
@endforeach
@endsection
