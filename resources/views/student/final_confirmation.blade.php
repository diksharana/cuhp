@extends('layouts.welcome_registration')

@section('content')

<div class="registration-form">
    
    <ul id="progressbar">
        <li>
          <span>Course Details</span>
        </li>
        <li>
          <span>Personal Information</span>
        </li>
        <li>
          <span>Academic Information</span>
        </li>
        <li class="active">
          <span>Documentation</span>
        </li>
        <li>
          <span>Payment</span>
        </li>
    </ul>
    
    <div class="final_confirmation">
        <h2>Final Step</h2>
        <p>Before moving on to the final step for payment.</p>
        <label>
            <input type="checkbox" />
            Please confirm all the information entered by you is as per the application and correct.
        </label>
        <a href="" class="btn">Submit</a>
    </div>
    
</div>

<!--
<div class="final_success_msg">
    <figure></figure>
    <h2>Your application has been submitted successfully.</h2>
    <a href="/student" class="btn">Go back to dashboard</a>
</div>
-->

@endsection
