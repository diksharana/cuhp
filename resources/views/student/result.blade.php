@extends('layouts.student')

@section('content')
    {{-- <a class="icon-previous" href="{{ URL::previous() }}" title="Previous"></a> --}}
    @if ($result)
      <h2 class="content-title">Results</h2>
      @if (session()->has('message'))
          <h5>{{ session('message') }}</h5>
      @endif
      <ul class="info-list">
          @foreach ($result as $key => $value)
            <li>
                {{-- <a href="">UG & Certificate Programmes</a> --}}
                <label>{{$value->form_name}}</label>
                <a target="_blank" href="{{$value->url}}">Click here for details</a>
            </li>
          @endforeach
      </ul>
    @else
      <p class="info-text">
          No information available.
          <span>Information will be updated here as soon as admin update it.</span>
      </p>
    @endif
@endsection
