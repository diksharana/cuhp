@extends('layouts.student')

@section('content')

<a class="icon-previous" href="{{ URL::previous() }}" title="Previous"></a>
<h2 class="content-title">Attendance</h2>
@foreach ($attendance as $key => $value)
    <a target="_blank" href="{{$value->url}}">{{$value->form_name}}</a>
@endforeach

@endsection
