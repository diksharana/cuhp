<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Admit Card</title>
  <style>
      body {
          font-family: 'Lato', sans-serif;
          margin: 0;
          padding: 0;
      }

      .table {
          width: 800px;
          margin: 50px auto;
      }

      .table thead tr th {
          text-align: center;
          background: #f1f1f1;
      }

      .table thead tr th img {
          float: left;
          width: 100px;
          margin: 15px 0 15px 100px;
      }

      .table thead tr th span {
          float: left;
          font-size: 28px;
          line-height: 34px;
          color: #009A47;
          margin: 45px 0 0 30px;
      }
      
      .table tbody tr td {
          border: 1px solid #212121;
      }

      .title-header {
          text-align: center;
      }

      .title-header span {
          float: left;
          width: 100%;
          font-size: 22px;
          line-height: 26px;
          font-weight: 700;
          color: #212121;
          margin: 0;
          padding: 15px;
          text-align: center;
      }

      .student-info td:first-child {
          border-right: 0;
      }

      .student-info td:last-child {
          border-left: 0;
      }

      .student-info ul {
          width: 580px;
          list-style-type: none;
          margin: 0;
          padding: 0;
          border-right: 1px solid #212121;
      }

      .student-info ul li {
          margin: 0;
          padding: 10px;
          border-top: 1px solid #212121;
      }

      .student-info ul li.no-border {
          border: 0;
      }

      .student-info ul li label {
          font-size: 14px;
          line-height: 20px;
          font-weight: 700;
          margin-right: 10px;
      }

      .student-info ul li span {
          font-size: 14px;
          line-height: 20px;
      }

      .student-info td+td {
          width: 200px;
      }

      .student-info td+td span {
          padding: 15px;
      }

      .student-info td+td span img {
          width: 150px;
      }

      .exam-info ul {
          list-style-type: none;
          margin: 0;
          padding: 0;
      }

      .exam-info ul li {
          float: left;
          width: 174px;
          margin: 0;
          padding: 10px;
          border-right: 1px solid #212121;
      }

      .exam-info ul li.no-border {
          border: 0;
      }

      .exam-info ul li label {
          display: block;
          font-size: 14px;
          line-height: 20px;
          font-weight: 700;
          margin-bottom: 15px;
      }

      .exam-info ul li span {
          display: block;
          padding: 0 0 5px;
      }

  </style>
</head>
<body>
	<h1>  CUHP </h1>
	   <table class="table" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th colspan="2">
                        {{-- <img src="{{public_path() .'/images/cu-hp.png'}}" alt="" /> --}}
                        <img src="{{asset('/images/cu-hp.png')}}" alt="" />
                        <span> Central University of Himachal Pradesh </span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr class="title-header">
                    <td colspan="2">
                        <span>Admit Card</span>
                    </td>
                </tr>
                <tr class="student-info">
                    <td>
                        <ul>
                            <li class="no-border">
                                <label>Student Name:</label>
                                <span>{{$data[0]->first_name}}</span>
                                <span>{{$data[0]->last_name}}</span>
                            </li>
                            <li>
                                <label>Student ID:</label>
                                <span>{{$data[0]->userid}} </span>
                            </li>
                            <li>
                                <label>Date of Birth:</label>
                                <span>{{$data[0]->DOB}}</span>
                            </li>
                            <li>
                                <label>Course:</label>
                                <span>{{$data[0]->course}}</span>
                            </li>
                            <li>
                                <label>Centre Code:</label>
                                <span> {{$data[0]->centre_code}} </span>
                            </li>
                            <li>
                                <label>Centre Address:</label>
                                <span> {{$data[0]->centre_address}}</span>
                            </li>
                        </ul>
                    </td>
                    <td>
                        <span>
                          {{-- <img src="{{public_path() .$data[0]->photo}}" alt="{{$data[0]->photo}}" /> --}}
                            <img src="{{asset($data[0]->photo)}}" alt="{{$data[0]->photo}}" />
                        </span>
                    </td>
                </tr>
                <tr class="exam-info">
                    <td colspan="2">
                        <ul>
                            <li>
                                <label>Date of Examination</label>
                                <span>  {{$data[0]->date_of_examination}} </span>
                            </li>
                            <li>
                                <label>Examination Timing</label>
                                <span> {{$data[0]->examination_timings}}</span>
                            </li>
                            <li>
                                <label>Reporting Time</label>
                                <span> {{$data[0]->reporting_time}}</span>
                            </li>
                            <li class="no-border">
                                <label>Examination Controller</label>
                                <span>{{$data[0]->examination_controller}} </span>
                            </li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>

</body>
</html>
