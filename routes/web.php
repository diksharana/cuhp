<?php

use App\Events\MessageCreated;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/cuhp-admin', 'AdminController@showAdminLoginForm');
Route::post('/cuhp-admin/login', 'AdminController@admin_dashboard');
Route::get('/cuhp-admin/dashboard', 'AdminController@admin_dashboard');
Route::get('/cuhp-admin/forms', 'AdminController@admin_forms');
Route::get('/cuhp-admin/exam_schedule', 'AdminController@admin_datesheet');
Route::get('/cuhp-admin/result', 'AdminController@admin_result');
Route::get('/cuhp-admin/send_emails', 'AdminController@send_emails');
Route::post('/cuhp-admin/admitcard', 'AdminController@admin_admitcard');
Route::get('/cuhp-admin/user_admit', 'AdminController@admin_useradmitcard');
Route::get('/cuhp-admin/approved', 'AdminController@approved');
Route::get('/cuhp-admin/view_application/{id}', 'AdminController@view_application');
Route::get('/cuhp-admin/rejected', 'AdminController@rejected');
Route::get('/cuhp-admin/attendance', 'AdminController@admin_attendance');
Route::get('/cuhp-admin/users', 'AdminController@admin_users');
Route::post('/publish', 'AdminController@admit_card_publish');
Route::get('/cuhp-admin/verify', 'AdminController@verify');
Route::post('/cuhp-admin/send_emails/complete_process', 'AdminController@email_to_complete_process');
Route::post('/complete_detail', 'AdminController@complete_detail');
// Route::post('/cuhp-admin/verify', 'AdminController@verify');
Route::post('verify_teacher', 'AdminController@verify_teacher');
Route::post('verify_management', 'AdminController@verify_management');

Route::post('/schedule', 'AdminController@schedule');
Route::get('/generate-pdf','AdminController@generatePDF');
Route::post('/approve/admitcard','AdminController@approve_admitcard');
Route::post('/reject_student','AdminController@reject_student');
Route::post('/approve_student','AdminController@approve_student');


// Route::get('/register/auth/admin', 'Auth\RegisterController@showAdminRegisterForm');
// Route::post('/register/auth/admin', 'Auth\RegisterController@createAdmin');

Route::get('/login/student', 'Auth\LoginController@showStudentLoginForm');
Route::post('/login/auth/student', 'Auth\LoginController@StudentLogin');
Route::get('/login/auth/student', 'Auth\LoginController@StudentLogin');
Route::get('/register/auth/student', 'Auth\RegisterController@showStudentRegisterForm');
Route::post('/register/auth/student', 'Auth\RegisterController@createStudent');
Route::get('/register/detail', 'StudentController@fillDetail');
Route::post('/register/detail', 'StudentController@fillDetail');

Route::get('/login/teacher', 'Auth\LoginController@showTeacherLoginForm');
Route::post('/login/auth/teacher', 'Auth\LoginController@TeacherLogin');
Route::get('/register/auth/teacher', 'Auth\RegisterController@showTeacherRegisterForm');
Route::post('/register/auth/teacher', 'Auth\RegisterController@createTeacher');

Route::get('/login/management', 'Auth\LoginController@showManagementLoginForm');
Route::post('/login/auth/management', 'Auth\LoginController@managementLogin');
Route::get('/register/auth/management', 'Auth\RegisterController@showManagementRegisterForm');
Route::post('/register/auth/management', 'Auth\RegisterController@createManagement');


//student
Route::prefix('student')->group(function () {
   Route::post('/', 'StudentController@final_step');
   Route::get('/', 'StudentController@index');
   Route::get('/profile', 'StudentController@profile');
   Route::post('/profile', 'StudentController@profile');
   Route::get('/attendance', 'StudentController@attendance');
   Route::get('/result', 'StudentController@result');
   Route::get('/courses', 'StudentController@courses');
   Route::get('/achievements', 'StudentController@achievements');
   Route::get('/assignments', 'StudentController@assignments');
   Route::get('/dates', 'StudentController@dates');
   Route::get('/admission', 'StudentController@admission');
   Route::get('/timetable', 'StudentController@timetable');
   Route::get('/calendar', 'StudentController@calendar');
   Route::get('/messages', 'StudentController@messages');
   Route::get('/new_mail','StudentController@new_mail');
   Route::get('/inbox','StudentController@inbox');
   Route::get('/draft','StudentController@draft');
   Route::get('/history','StudentController@history');
   Route::get('/other', 'StudentController@other');
   Route::post('/payment', 'StudentController@payment');
});

Route::get('/paymentresponse', 'StudentController@paymentresponse');
Route::get('/student/messages', 'ChatController@index')->name('chat')->middleware('auth:student');
Route::get('indipay/response', 'StudentController@indipayresponse');
Route::get('/final_confirmation', 'StudentController@final_confirmation');



Route::get('/messages/{senderid}', function($senderid) {
  $user_id = Auth::guard('student')->user()->id;
  // var_dump(Auth::guard('student')->user()->id).die();
  return App\Message::with('user')->where(['user_id'=>$user_id,'student_id'=>$user_id,'sender_id'=>$senderid])->get();

})->middleware('auth:student');


Route::post('/messages','ChatController@message')->middleware('auth:student');
Route::post('/message_userid','ChatController@message_userid')->middleware('auth:student');


Route::get('/users', 'ChatController@users')->middleware('auth:student');

Route::get('/teacher', 'TeacherController@index');
Broadcast::routes(['middleware' => 'auth:student']);

Route::get('/management', function(){
  return view('management')->middleware('verified');
});



//common calls for all dashboards
Route::post('/profile_save_data', 'HomeController@profile_save_data');
Route::post('/mail_sent', 'HomeController@mail_sent');



//new phase-1 project
Route::get('/register/student', 'StudentController@register');
Route::get('/save_new_student', 'StudentController@documents');
Route::post('/save_documents', 'StudentController@save_documents');
Route::get('/register/forms_student', 'StudentController@forms_student');

Route::get('/new_student_profile_save_data', 'StudentController@save_data_new_student');




Route::post('/register/send-sms','SmsController@store');
Route::get('/register/verify-user','SmsController@verifyContact');
Route::get('/view_application','StudentController@view_application');
