<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('name')->default('abc');
          // $table->date('DOB')->nullable();
          $table->string('email')->unique();
          $table->string('password');
          $table->string('phone_number')->unique();
          $table->string('state')->nullable();
          // $table->string('city')->nullable();
          $table->string('course')->nullable();
          $table->string('status')->default('registered');
          $table->string('remember_token')->nullable();
          $table->timestamps();
        });
      }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
