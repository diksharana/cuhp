<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmitcardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admitcards', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('student_id')->nullable();
          $table->string('centre_code')->nullable();
          $table->string('centre_address')->nullable();
          $table->string('date_of_examination')->nullable();
          $table->string('examination_timings')->nullable();
          $table->string('reporting_time')->nullable();
          $table->string('examination_controller')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admitcards');
    }
}
