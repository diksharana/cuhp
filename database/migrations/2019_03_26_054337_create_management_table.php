<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('managements', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('email')->unique();
        $table->string('password');
        $table->string('avatar')->default('sample.jpg');
        $table->string('designation')->default('NULL');
        $table->boolean('is_management')->default(false);
        $table->string('roll_number')->unique();
        $table->string('phone_number')->unique()->default('default');
        $table->timestamp('email_verified_at');
        $table->rememberToken();
        // $table->string('token_2fa')->nullable();
        // $table->datetime('token_2fa_expiry')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management');
    }
}
