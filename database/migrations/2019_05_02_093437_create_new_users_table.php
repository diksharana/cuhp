<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_users', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('program_type')->nullable();
          $table->string('faculty')->nullable();
          $table->string('course')->nullable();
          $table->string('subject')->nullable();
          $table->string('first_name')->nullable();
          $table->string('middle_name')->nullable();
          $table->string('last_name')->nullable();
          $table->date('DOB')->nullable();
          $table->string('gender')->nullable();
          $table->string('address_line1')->nullable();
          $table->string('address_line2')->nullable();
          $table->string('city')->nullable();
          $table->string('state')->nullable();
          $table->string('code')->nullable();
          $table->string('country')->nullable();
          $table->string('address_line1_correspondance')->nullable();
          $table->string('address_line2_correspondance')->nullable();
          $table->string('city_correspondance')->nullable();
          $table->string('state_correspondance')->nullable();
          $table->string('code_correspondance')->nullable();
          $table->string('country_correspondance')->nullable();
          $table->string('email')->unique();
          $table->string('phone_number')->unique()->nullable();
          $table->string('mother_name')->nullable();
          $table->string('father_name')->nullable();
          $table->string('parents_phone')->nullable();
          $table->string('highest_qualification')->nullable();
          $table->string('percentage_highest_qualification')->nullable();
          $table->string('photo')->nullable();
          $table->string('adhar_card')->nullable();
          $table->string('signature')->nullable();
          $table->string('marksheet_10th')->nullable();
          $table->string('marksheet_12th')->nullable();
          $table->string('status')->nullable();
          $table->string('reason')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_users');
    }
}
