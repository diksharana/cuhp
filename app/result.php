<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class result extends Model
{
  protected $fillable = [
      'course_name', 'file',
  ];
}
