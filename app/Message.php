<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = ['message','user_id','student_id','sender_id'];

    // protected $appends = ['selfMessage'];

    // public function getSelfMessageAttribute()
    // {
    //     return $this->user_id === auth()->guard('student')->user()->id;
    // }

    public function user()
    {
        return $this->belongsTo(Student::class);
    }
}
