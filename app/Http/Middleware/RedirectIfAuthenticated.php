<?php

   namespace App\Http\Middleware;

   use Closure;
   use Illuminate\Support\Facades\Auth;

   class RedirectIfAuthenticated
   {
       public function handle($request, Closure $next, $guard = null)
       {
       //     if ($guard == "admin" && Auth::guard($guard)->check()) {
       //         return redirect('/admin');
       //     }
           if($guard == "student" && Auth::guard($guard)->check()) {
             //dd($guard);
               return redirect('/student');
           }
           // elseif ($guard == "teacher" && Auth::guard($guard)->check()) {
           //     return redirect('/teacher');
           // }
           // elseif ($guard == "management" && Auth::guard($guard)->check()) {
           //     return redirect('/management');
           // }
           // elseif (Auth::guard($guard)->check()) {
           //     return redirect('/home');
           // }
           return $next($request);
       }
   }
