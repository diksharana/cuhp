<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use App\Events\MessageCreated;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::all();
        // var_dump($messages).die();
        return response()->json($messages);

    }

    public function store(Request $request)
    {
      // var_dump($request->user('student')).die();
        $message = $request->user('student')->messages()->create([
            'body' => $request->body
        ]);

        broadcast(new MessageCreated($message))
                ->toOthers();
                // var_dump($message).die();
        return response()->json($message);
    }
}
