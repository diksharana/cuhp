<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Student;
use App\forms;
use App\Http\Requests;
use App\result;
use App\attendance;
use App\User;
use App\course;
use App\new_user;
use App\admitcard;
use App\mail_to_admin;
use App\datesheet;
use App\timetable;
use Auth;
use DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Softon\Indipay\Facades\Indipay;
use App\Mail\completed_regprocess;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request) {
      $user = Auth::user();
      // $student = User::where(['email'=>$user->email])->get();
      $student = DB::table('new_users')
              ->join('users', 'new_users.userid', '=', 'users.id')
              ->where('users.email','=',$user->email)
              ->get();
      if ($student->isEmpty()) {
        $student = User::where(['email'=>$user->email])->get();
      }
      return view('student.student')->with('data', $student);
    }
    public function final_step(Request $request) {
      $user = Auth::user();
      if ($request->declaration=="checked") {
        DB::table('new_users')
              ->where('userid', $user->id)
              ->update([
                'step' => 5,
                'declaration' => "checked",
                'status' => "registered",
                'updated_at' => now()
              ]);
          DB::table('users')
                ->where('id', $user->id)
                ->update([
                  'status' => "applied",
                  'updated_at' => now()
                ]);
        $student = User::where(['email'=>$user->email])->get();
        try {
          Mail::to($user->email)->send(new completed_regprocess());
        } catch (\Exception $e) {
            $failed_email = array();
            $failed_email['email'] = $registered_user->email;
            $failed_email['reason'] = $e->getMessage();
            $failed_email['created_at'] = now();
            DB::table('failedmails')->insert($failed_email);
        }
        return view('student.student')->with('data', $student);
      }
      else{
        return back();
      }
    }
    public function indipayresponse(Request $request) {

        // For Otherthan Default Gateway
        $response = Indipay::gateway('InstaMojo')->response($request);

        dd($response);

    }
    public function profile (Request $request) {
      $user = Auth::user();
      $student = User::where(['email'=>$user->email])->get();
      $profile_image = '';
      $profile_image = new_user::select('photo')->where(['email'=>$user->email])->first();
      if ($profile_image) {
        $profile_image = $profile_image->photo;
      }
      return view('student.profile')->with(['data'=> $student,'profile_image'=> $profile_image]);
    }
    public function attendance(Request $request) {
      $attendance = attendance::all();
      return view('student.attendance')->with('attendance',$attendance);
    }
    public function assignments(Request $request) {
      return view('student.assignments');
    }
    public function calendar(Request $request) {
      return view('student.calendar');
    }
    public function result(Request $request) {
      $result = result::all();
      return view('student.result')->with('result', $result);
    }
    public function courses(Request $request) {
      $courses = admitcard::where(['student_id'=>Auth::user()->id])->whereNotNull('centre_code')->get();
      return view('student.courses')->with('courses', $courses);
    }
    public function admission(Request $request) {
      return view('student.admission');
    }
    public function dates(Request $request) {
      $datesheet = datesheet::all();
      return view('student.examSchedule')->with('datesheet', $datesheet);
    }
    public function timetable(Request $request) {
      $timetable = timetable::all();
      return view('student.timetable')->with('timetable', $timetable);
    }
    public function achievements(Request $request) {
      return view('student.achievements');
    }
    public function messages(Request $request) {
      $students = Student::all();
      //var_dump($students).die();
       return view('student.messages');
    }
    public function other(Request $request) {
      $forms = forms::where('related_to','=','Student')->get();
      return view('student.other')->with('forms',$forms);
    }
    public function new_mail()
    {
      return view('mail.new_mail');
    }
    public function history(){
      $mails = mail_to_admin::where(['status'=>'sent'])->get();
      return view('mail.sentmail')->with('mails', $mails);
    }
    public function inbox(){
      $mails = mail_to_admin::where(['status'=>'received'])->get();
      return view('mail.inbox')->with('mails', $mails);
    }
    public function draft(){
      $mails = mail_to_admin::where(['status'=>'drafts'])->get();
      return view('mail.drafts')->with('mails', $mails);
    }
    public function register(Request $request)
    {
      $step = 1;
      if ( !Auth::user()) {
        return redirect('/');
      }
      $user_detail = new_user::where('userid', '=', Auth::user()->id)->first();
      // if ($user_detail && $user_detail->step) {
      //    $step = $user_detail->step;
      // }
      // if (condition) {
      //   // code...
      // }
      return view('student.register.register')->with('user_detail',$user_detail);
    }
    public function documents(Request $request)
    {
      return view('student.register.student_document');
    }
    public function save_data_new_student(Request $request)
    {
      $temp[] = $request->all();
      // var_dump($temp).die();
      unset($temp[0]['_token']);
      $student_email = $request->input('email');
      $request->session()->put('email', $student_email);
      var_dump($request->session()->all()).die();
      $user = Student::create($temp[0]);
      //
      return back() ;
    }
    public function forms_student()
    {
      $user = Auth::user();
      $student = DB::table('new_users')
              ->join('users', 'new_users.userid', '=', 'users.id')
              ->where('users.email','=',$user->email)
              ->get();
      if ($student->isEmpty()) {
        $student = User::where(['email'=>$user->email])->get();
      }
      return view('student.register.student_forms')->with('data', $student);
    }
    public function fillDetail(Request $request) {
      $user_id = Auth::user()->id;
      $data = $request->all();
      // if (!$data['email']) {
      //   $data['email'] = Auth::user()->email;
      // }
      $data['status'] = 'draft'; //payment made and all details filled
      unset($data['_token']);
      $data['email'] = Auth::user()->email;
      // var_dump($data['middle_name']).die();
      $new_user = new_user::updateOrCreate(['userid'=>$user_id],$data);
      if ($request->step==4) {
        $request = $this->save_documents($request);
      }
      //$user = user::where(['email'=>$data['email']])->update(['status'=>'applied']);
    }
    public function save_documents(Request $request) {
      $file_src = '';
      $files  = array(
        'adhar_card' => $request->file('adhar_card'),
        'marksheet_10th' => $request->file('marksheet_10th'),
        'marksheet_12th' => $request->file('marksheet_12th'),
        'photo' => $request->file('photo'),
        'signature' => $request->file('signature')
      );
      foreach ($files as $key => $value) {
        $user_id = Auth::user()->id;
        if ($value) {
          $value->move('document',$value->getClientOriginalName());
          $file_src = "/document/".$value->getClientOriginalName();
          $request->$key = $file_src;
          $new_user = new_user::updateOrCreate(['userid'=>$user_id],[$key=>$file_src]);
        }
      }
      // var_dump($request).die();
      return $request;
    }
    public function payment(Request $request){
      $user = Auth::user();
      if ($user) {
        $userid = $user->id;
        $name = $user->name;
        $email = $user->email;
        $phone = $user->phone_number;
      }
      else {
        return redirect('/');
      }
      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/');
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
      curl_setopt($ch, CURLOPT_HTTPHEADER,
                  array("X-Api-Key:bb4b03a5a054956c0a4595756ee4d8f0",
                        "X-Auth-Token:493ddbc8ce04894eb0bf0ccdd5b18384"));
      $payload = Array(
          'purpose' => 'Application fee',
          'amount' => '500',
          'phone' => $phone,
          'buyer_name' => $name,
          'redirect_url' => 'http://www.cuhp.codemiles.in/paymentresponse/',
          'send_email' => false,
          'webhook' => 'http://www.cuhp.codemiles.in/webhook/',
          'send_sms' => false,
          'email' => $email,
          'allow_repeated_payments' => false
      );
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
      $response = curl_exec($ch);
      curl_close($ch);
      $response = json_decode($response);
      // save to Database
      $paymenturl = $response->payment_request->longurl;
      DB::table('payments')->insert(
        [
          'payment_request_id' => $response->payment_request->id,
          'userid' => $userid,
          'paymenturl' => $paymenturl,
          'created_at' => now(),
        ]
      );
      return redirect($paymenturl);
    }
    public function paymentresponse(Request $request){
      $payment_id = Input::get('payment_id');
      $payment_request_id = Input::get('payment_request_id');
      $payment_status = Input::get('payment_status');
      DB::table('payments')
            ->where('payment_request_id', $payment_request_id)
            ->update([
              'payment_id' => $payment_id,
              'status' => $payment_status,
              'updated_at' => now(),
            ]);
      return view('student.paymentresponse')->with('status',$payment_status);
    }
    public function view_application(Request $request) {
      $email = Auth::user()->email;
      $student = new_user::where(['email'=>$email])->first();
      if(!$student){
        return view('student.view_application')->with('message','No application found');
      }
      return view('student.view_application')->with('student',$student);
    }
    public function final_confirmation(){
      return view('student.final_confirmation');
    }
    public static function lastSlashString($str){
      $pos = strrpos($str, '/');
      $str = $pos === false ? $str : substr($str, $pos + 1);
      echo $str;
    }
}
