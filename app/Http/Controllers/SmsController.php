<?php
namespace App\Http\Controllers;
use Twilio\Jwt\ClientToken;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use app\SmsVerification;

class SmsController extends Controller
  {
    protected $code, $smsVerifcation;
    function __construct() {
     $this->smsVerifcation = new \App\SmsVerification();
    }
    public function store(Request $request) {
        $code = rand(1000, 9999); //generate random code
        $request['code'] = $code; //add code in $request body
        $s=$this->smsVerifcation->store($request); //call store method of model
        //return $this->sendSms($request); // send and return its response
        if ($this->sendSms($request)) {
          return 1;
        }
        else{
          return 0;
        }
        //return view('auth.mobile_verify', ['code' => $this->sendSms($request)]);
      }
    public function verifyContact(Request $request) {
      $value = $request->session()->get('contact');
      $smsVerifcation = $this->smsVerifcation::where('phone_number','=',$value)->latest()->first();
      // var_dump($request).die();
      if($request->code == $smsVerifcation->code) {
       $request["status"] = 'verified';
       $smsVerifcation->updateModel($request);
       $msg["message"] = "verified";
       //$writer = Auth::guard('student')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'));

       //return view('student.student_document');
      }
      else {
       $msg["message"] = "not verified";
       //return view('student.verify_otp')->with('msg', $msg);
      }
      return $msg["message"];
    }
    public function sendSms($request) {
     $accountSid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
     $authToken = config('app.twilio')['TWILIO_AUTH_TOKEN'];
     $request->session()->put('contact', $request->phone_number);
     try {
       $client = new Client(['auth' => [$accountSid, $authToken]]);
       $result = $client->post('https://api.twilio.com/2010-04-01/Accounts/'.$accountSid.'/Messages.json',
       ['form_params' => [
       'Body' => 'CODE: '. $request->code, //set message body
       // 'To' => '+91'.$request->contact_number,
       'To' => '+91'.$request->phone_number,
       'From' => '+12024101554' //we get this number from twilio
       ]]);
       // return $result;
       // // var_dump($result);
       //return view('student.verify_otp');
       return $result->getReasonPhrase();
     }
     catch (Exception $e)  {
       echo "Error: " . $e->getMessage();
     }
   }
}
