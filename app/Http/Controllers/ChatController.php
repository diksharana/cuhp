<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Teacher;
use App\Message;
use DB;
use Auth;
use App\Events\MessageCreated;


class ChatController extends Controller
{
    // public function __construct()
    // {
    //     return $this;
    // }

    public function index(Request $request) {
      $students = Student::get();
      $teachers = Teacher::get();
      return view('chat')->with('students',$students)->with('teachers',$teachers);
    }
    public function users(Request $request) {
      $students = Student::where('id', '!=' ,Auth::user()->id)->get();
      return $students;
    }
    public function message(Request $request){
      $user = Auth::guard('student')->user();
      $message = $user->messages()->create([
        'user_id' => $user->id,
        'sender_id' => '3',
        'message' => request()->get('message'),
      ]);
      broadcast(new MessageCreated($message, $user))->toOthers();
      return ['status' => 'ok'];
    }
    public function message_userid(Request $request) {
      $student = $request->get('student_id');
      $messages = Message::where('student_id','=',$student);
      return $messages ;
    }
    public function chat_of_student(Request $request) {
      $student = $request->get('student');
      $user = Auth::guard('student')->user();
      $user_id = $user->id;
      $message = Message::where(['user_id'=>$user_id,'student_id'=>$student])->get();
      var_dump($message);
      return $message;
    }
}
