<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Admin;
use App\Management;
use App\Teacher;
use Illuminate\Support\Facades\Mail;
use App\Mail\registered_email;
use App\Student;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:admin');
        $this->middleware('guest:student');
        $this->middleware('guest:teacher');
        $this->middleware('guest:management');
    }
    // public function showAdminRegisterForm() {
    //   return view('auth.register', ['url' => 'auth/admin']);
    // }
    public function showStudentRegisterForm() {
      return view('auth.register', ['url' => 'auth/student']);
    }
    // public function showTeacherRegisterForm() {
    //   return view('auth.register', ['url' => 'auth/teacher']);
    // }
    // public function showManagementRegisterForm() {
    //   return view('auth.register', ['url' => 'auth/management']);
    // }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255',  'unique:users','unique:students'],
            'phone_number' => ['required', 'unique:users', 'unique:students'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    // protected function create(array $data) {
    //     return User::create([
    //         'name' => $data['name'],
    //         'email' => $data['email'],
    //         'password' => Hash::make($data['password']),
    //         'phone_number' => 'default',
    //         'roll_number' => $request['roll_number'],
    //     ]);
    // }
    protected function createStudent(Request $request) {
      // var_dump($request['name']).die();
      // dd($this->validator($request->all())->validate());
      $this->validator($request->all())->validate();
      $writer = User::create([
          'name' => $request['name'],
          'email' => $request['email'],
          'phone_number' => $request['phone_number'],
          'password' => Hash::make($request['password']),
          'course' => $request['course'],
          'state' => $request['state'],
          'is_student' => '1',
          'created_at' => now()
      ]);
      $writer = Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'));
      if ($writer == 'true') {
        Mail::to($request->email)->send(new registered_email());
        $user = Auth::user();
        $student = User::where(['email'=>$user->email])->get();
        return view('student.student')->with('data', $student);
      }
      else{
        return redirect()->back()->with('auth-message', 'Error in Registration');
      }
    }
    // protected function createTeacher(Request $request) {
    //   $this->validator($request->all())->validate();
    //   // var_dump('123').die();
    //   $writer = Teacher::create([
    //       'name' => $request['name'],
    //       'email' => $request['email'],
    //       'phone_number' => 'default',
    //       'is_teacher' => '1',
    //       'roll_number' => $request['roll_number'],
    //       'created_at' => now(),
    //       'password' => Hash::make($request['password']),
    //   ]);
    //   if(!empty($writer))  {
    //     return back()->with('errors','yo');
    //   }
    //   // var_dump(empty($writer)).die();
    //   return redirect()->intended('login/teacher');
    // }
    // protected function createManagement(Request $request) {
    //   $this->validator($request->all())->validate();
    //   $writer = Management::create([
    //       'name' => $request['name'],
    //       'email' => $request['email'],
    //       'phone_number' => 'default',
    //       'is_management' => '1',
    //       'roll_number' => $request['roll_number'],
    //       'created_at' => now(),
    //       'password' => Hash::make($request['password']),
    //   ]);
    //   return redirect()->intended('login/management');
    // }
    // protected function createAdmin(Request $request) {
    //   $this->validator($request->all())->validate();
    //   $writer = Admin::create([
    //       'name' => $request['name'],
    //       'email' => $request['email'],
    //       'password' => Hash::make($request['password']),
    //   ]);
    //   return redirect()->intended('login/admin');
    // }
}
