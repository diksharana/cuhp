<?php

namespace App\Http\Controllers\Auth;
// namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Carbon;
use Auth;
use DB;

class LoginController extends Controller {
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

  /**
   * Where to redirect users after login.
   *
   * @var string
   */
  protected $redirectTo = '/home';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()  {
    $this->middleware('guest')->except('logout');
    $this->middleware('guest:admin')->except('logout');
    $this->middleware('guest:student')->except('logout');
    $this->middleware('guest:teacher')->except('logout');
    $this->middleware('guest:management')->except('logout');
  }
  public function logout(Request $request) {
    Auth::guard('student')->logout();
    Auth::guard('teacher')->logout();
    Auth::guard('management')->logout();
    Auth::guard('admin')->logout();
    $request->session()->flush();
    $request->session()->regenerate();
    return redirect('/');
  }
  public function showAdminLoginForm() {
    return view('auth.login', ['url' => 'auth/admin']);
  }
  public function adminLogin(Request $request)  {
    $this->validate($request, [
        'roll_number'   => 'required|',
        'password' => 'required|min:6'
    ]);
    if (Auth::guard('admin')->attempt(['roll_number' => $request->roll_number, 'password' => $request->password], $request->get('remember'))) {
      return view('admin.admin');
    }
    return back()->withInput($request->only('roll_number', 'remember'));
  }
  public function showStudentLoginForm() {
    return view('auth.login', ['url' => 'auth/student']);
  }
  public function StudentLogin(Request $request)  {
    $this->validate($request, [
        'email'   => 'required|',
        'password' => 'required|min:6'
    ]);
    $writer = Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'));
    if ($writer == 'true') {
      $user = Auth::user();
      $student = DB::table('new_users')
              ->join('users', 'new_users.userid', '=', 'users.id')
              ->where('users.email','=',$user->email)
              ->get();
      if ($student->isEmpty()) {
        $student = User::where(['email'=>$user->email])->get();
      }
      return view('student.student')->with('data', $student);
    }
    else {
      return back()->with('login-message', 'Wrong Credentials');
  }
  // public function showTeacherLoginForm() {
  //   return view('auth.login', ['url' => 'auth/teacher']);
  // }
  // public function TeacherLogin(Request $request) {
  //   $this->validate($request, [
  //     'roll_number'   => 'required|',
  //     'password' => 'required|min:6'
  //   ]);
  //   if (Auth::guard('teacher')->attempt(['roll_number' => $request->roll_number, 'password' => $request->password], $request->get('remember'))) {
  //     return view('teacher.teacher');
  //   }
  //   return back()->withInput($request->only('roll_number', 'remember'));
  // }
  // public function showManagementLoginForm() {
  //   return view('auth.login', ['url' => 'auth/management']);
  // }
  // public function ManagementLogin(Request $request) {
  //   // var_dump($request->password).die();
  //   $this->validate($request, [
  //     'roll_number'   => 'required|',
  //     'password' => 'required|min:6'
  //   ]);
  //   if (Auth::guard('management')->attempt(['roll_number' => $request->roll_number, 'password' => $request->password], $request->get('remember'))) {
  //     return view('management.management');
  //
  //     // return redirect()->intended('/management');
  //   }
  //   return back()->withInput($request->only('roll_number', 'remember'));
  // }

}
}
