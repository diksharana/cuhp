<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\mail;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:student');
    }
    public function profile_save_data(Request $request)
    {
      $name = $request->input('name');
      $email = $request->input('email');
      $roll_number = $request->input('roll_number');
      $phone_number = $request->input('phone_number');
      $course = $request->input('course');
      $semester = $request->input('semester');
      $address = $request->input('address');
      $DOB = $request->input('DOB');
      $father = $request->input('father_name');
      $mother = $request->input('mother_name');
      $reg_no = $request->input('reg_no');
      $parents_phone = $request->input('parents_phone');
      $table = '\App\\'.$request->input('table');
      $model =  new $table();
      $model->where(['roll_number'=>$roll_number])->update(['name'=>$name,
        'father_name'=>$father,
        'mother_name'=>$mother,
        'reg_no'=>$reg_no,
        'parents_phone'=>$parents_phone,
        'course'=>$course,
        'email'=>$email,
        'DOB'=>$DOB,
        'semester'=>$semester,
        'address'=>$address,
        'phone_number'=>$phone_number]);
      $data = $model->where(['roll_number'=>$roll_number])->get();
      return back()->with('data', $data);
    }
    public function mail_sent(Request $request) {
      $mail_data = new mail;
      // var_dump($request->cc2).die();
      $mail_data->to = $request->to;
      $mail_data->cc = $request->cc2;
      $mail_data->subject = $request->subject;
      $mail_data->body = $request->mail_body;
      $mail_data->save();
      return back();
    }
    public function get_mail($value=''){
      $mails = mail::get();
      return view('mail.sentmail')->with('mails', $mails);
    }

}
