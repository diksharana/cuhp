<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Mail\app_aproved;
use App\Mail\rejection;
use App\Mail\entrance_exam;
use App\Mail\entrance_result;
use App\Mail\complete_process;
use App\Mail\download_admitcard;
use PDF;
use App\forms;
use App\Student;
use App\datesheet;
use App\admitcard;
use App\course;
use App\User;
use App\new_user;
use App\result;
use App\timetable;
use DB;
use Auth;
use Validator;

class AdminController extends Controller {
  public function showAdminLoginForm(Request $request)
  {
    return view('admin.admin');
  }
  public function admin_dashboard(Request $request)
  {
    if ($request->input('email') == 'admin@cuhp.com' && $request->input('password') == '00admin') {
      return view('admin.dashboard');
    }
    else {
      return back()->with('message','Wrong credentials');
    }
  }
  // public function admin_forms(Request $request)
  // {
  //   return view('admin.forms');
  // }
  public function admin_datesheet(Request $request)
  {
    $datesheets = datesheet::get();
    return view('admin.datesheet')->with('result',$datesheets);
  }
  public function admin_result(Request $request)
  {
    $results = result::get();
    return view('admin.result')->with('result',$results);;
  }
  public function admin_admitcard(Request $request)
  {
    $data = DB::table('new_users')
            ->join('admitcards', 'new_users.userid', '=', 'admitcards.student_id')
            ->where('admitcards.student_id','=',$request->student_id)
            ->get();
    if (!count($data)) {
      $data = new_user::where('userid','=',$request->student_id)->get();
    }
    return view('admin.admitcard')->with('data', $data);
  }
  public function approved(Request $request)
  {
    $data = DB::table('new_users')
            ->where('status','=','approved')
            ->get();
    return view('admin.approved')->with('data', $data);
  }
  public function rejected(Request $request)
  {
    $data = DB::table('new_users')
            ->where('status','=','rejected')
            ->get();
    return view('admin.rejected')->with('data', $data);
  }
  public function admin_useradmitcard(Request $request)
  {
    $data = DB::table('new_users')
            ->join('admitcards', 'new_users.userid', '=', 'admitcards.student_id')
            ->where('new_users.status','=','approved')
            ->get();

    return view('admin.timetable')->with('data', $data);
  }
  public function admit_card_publish(Request $request)
  {
    $data = $request->all();
    $save_admit_card = new admit_card;
    $save_admit_card->updateOrCreate(['student_id'   => $data['student_id']],$data);
    // Mail::to('')->send(new admit_card());
    // var_dump($data).die();
    return redirect('/cuhp-admin/user_admit');;
  }
  public function admin_users(Request $request)
  {
    $registered_users = User::where(['status'=>'registered'])->get();
    $applied_users = User::where(['status'=>'applied'])->get();
    return view('admin.user_detail')->with(['registered_users'=> $registered_users,'applied_users'=> $applied_users]);
  }
  // public function save_form(Request $request) {
  //   $file_src = '';
  //   $save_form = new forms;
  //   $save_form->form_name = $request->form_name;
  //   $save_form->related_to = $request->related_to;
  //   $file = $request->file('url');
  //   $file->move('forms',$file->getClientOriginalName());
  //   $file_src = "/forms/".$file->getClientOriginalName();
  //   $save_form->url = $file_src;
  //   $save_form->save();
  //   $data[] = new_user::select('email')->where('status','=','approved')->get();
  //   $emails[] = $data[0][0]->email;
  //   foreach ($emails as $key => $value) {
  //     Mail::to($value)->send(new entrance_result());
  //   }
  //   return back();
  // }
  public function schedule(Request $request)
  {
    $file_src = '';
    $folder = $request->input('data');
    $table = '\App\\'.$folder;
    $model = new $table();
    $model->form_name = $request->form_name;
    $file = $request->file('url');
    $file->move($folder,$file->getClientOriginalName());
    $file_src = "/".$folder."/".$file->getClientOriginalName();
    $model->url = $file_src;
    $request->url = $file_src;
    $messages = ['unique' => 'The file with the same name already exists.'];
    Validator::make(
      ['url'=>$request->url],
      ['url' => 'required|unique:'.$folder.'s'],
      $messages
    )->validate();
    $model->save();
    //mail to user for their exam date
    $data = new_user::select('email')->where('status','=','approved')->get();
    if (!$data->isEmpty()) {
      foreach ($data as $key => $value) {
        try {
          Mail::to($value->email)->send(new entrance_exam());
        } catch (\Exception $e) {
            $failed_email = array();
            $failed_email['email'] = $registered_user->email;
            $failed_email['reason'] = $e->getMessage();
            $failed_email['created_at'] = now();
            DB::table('failedmails')->insert($failed_email);
        }
      }
    }
    return back()->with('message', 'File uploaded successfully');
  }
  public function complete_detail(Request $request)
  {
    $user_id = $request->input('value');
    // $table = '\App\\'.$request->input('value');
    // $model =  new $table();
    // var_dump($user_id).die();
    $data = new_user::where(['email'=>$user_id])->get();
    // var_dump($data).die();
    return $data;
  }
  public function generatePDF()
    {
        $data = DB::table('new_users')
                ->join('admitcards', 'new_users.userid', '=', 'admitcards.student_id')
                ->where('admitcards.student_id','=',Auth::user()->id)
                ->get();
        // $admin_data = admit_card::get();
        // return view('myPDF')->with('data', $data);
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('myPDF',['data'=>$data])->setPaper('a4', 'landscape');
        return $pdf->download('admitcard.pdf');
    }
    public function approve_admitcard(Request $request)
    {
      $temp = $request->all();
      unset($temp['_token']);
      $user = admitcard::updateOrCreate(['student_id'=>$temp['student_id']],$temp);
      // Mail::to('diksha.rana@amakein.com')->send(new registered_email());
      if ($user) {
        try {
          Mail::to($temp['email'])->send(new download_admitcard());
        } catch (\Exception $e) {
            $failed_email = array();
            $failed_email['email'] = $registered_user->email;
            $failed_email['reason'] = $e->getMessage();
            $failed_email['created_at'] = now();
            DB::table('failedmails')->insert($failed_email);
        }
        return "success";
      }
      return "error";
      //return redirect('/cuhp-admin/user_admit');
    }
    public function reject_student(Request $request)
    {
      $reason = $request->input('reason');
      $email = $request->input('email');
      new_user::where(['email'=>$email])->update(['status'=>'rejected','reason'=>$reason,'step'=>1]);
      user::where(['email'=>$email])->update(['status'=>'rejected']);
      Mail::to($email)->send(new rejection($reason));
    }
    public function approve_student(Request $request)
    {
      $email = $request->input('email');
      new_user::where(['email'=>$email])->update(['status'=>'approved','reason'=>'approved']);
      user::where(['email'=>$email])->update(['status'=>'approved']);
      $new_user = new_user::where(['email'=>$email])->first();

      $admitcard = new admitcard;

        $admitcard->student_id = $new_user->userid;
        $admitcard->save();
      Mail::to($email)->send(new app_aproved());
    }
    public function view_application($id){
      $student = new_user::where(['id'=>$id])->first();
      if(!$student){
        return view('admin.view_application')->with('message','No application found');
      }
      return view('admin.view_application')->with('student',$student);
    }
    public function send_emails(Request $request)
    {
      return view('admin.send_emails');
    }
    public function email_to_complete_process(Request $request)
    {
      $registered_users = user::where(['status'=>'registered'])->get();
      $failed_emails = array();$failed_email = array();
      foreach ($registered_users as $key => $registered_user) {
        try {
          Mail::to($registered_user->email)->send(new complete_process());
        } catch (\Exception $e) {
            $failed_email['email'] = $registered_user->email;
            $failed_email['reason'] = $e->getMessage();
            $failed_email['created_at'] = now();
            $failed_emails[] = $failed_email;
        }
      }
      if (count($failed_emails)) {
        DB::table('failedmails')->insert([$failed_emails]);
      }
      echo 'ok';
    }
}
