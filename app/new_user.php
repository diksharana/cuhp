<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class new_user extends Model
{

  protected $fillable = [
    'userid','program_type','faculty','course','subject','first_name','middle_name','last_name','DOB','gender',
    'address_line1','address_line2','city','state',
    'code','country','address_line1_correspondance',
    'address_line2_correspondance','city_correspondance',
    'state_correspondance','code_correspondance','country_correspondance',
    'email','phone_number','mother_name','father_name','parents_phone',
    'highest_qualification','percentage_highest_qualification','photo',
    'adhar_card','signature','marksheet_10th','marksheet_12th','step','status','reason'
  ];
}
