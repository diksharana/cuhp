<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class datesheet extends Model
{
  protected $fillable = [
      'course_name', 'file',
  ];
}
