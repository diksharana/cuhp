<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class timetable extends Model
{
  protected $fillable = [
    'course_name', 'file',
  ];
}
