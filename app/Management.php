<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Management extends Authenticatable
{
    use Notifiable;

    protected $guard = 'management';

    protected $fillable = [
        'name', 'email', 'password','roll_number'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
