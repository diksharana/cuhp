<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admitcard extends Model
{
    protected $table='admitcards';
    protected $fillable = [
        'student_id','centre_code','centre_address',
        'date_of_examination','examination_timings',
        'reporting_time','examination_controller'
    ];

}
