<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class admit_card extends Authenticatable
{
    use Notifiable;


    protected $fillable = [
        'name', 'student_id','centre_code','centre_address',
        'date_of_examination','examination_timings',
        'reporting_time','examination_controller'
    ];
}
