<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class forms extends Model
{

  protected $fillable = [
      'form_name', 'related_to', 'file',
  ];

}
